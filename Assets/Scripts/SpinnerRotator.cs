﻿/*Copyright (C) GameStudio316
*/
using UnityEngine;
using UnityEngine.UI;
using System;

namespace FastSpinner {
	public class SpinnerRotator : MonoBehaviour
	{
		#region Unity scene settings
		[SerializeField] float maxSpeed=50f;
		[SerializeField] float maxLackTime=10f;
		[SerializeField] float goldPeriod=5f;
		[SerializeField] Image image;
		#endregion

		#region Data
		float speed=0f;
		int _turns=0;
		float lastRot=0f;
		bool _play=false;
		Spinner spinner=null;
		float maxSpeedSec=0f;
		float lackTime=0f;
		float lastGoldAddTime = 0f;
		long lastGoldAddTurns = 0;
		#endregion

		#region Interface
		[HideInInspector] public float recordSpeed=0f;
		[HideInInspector] public float timeRot=0f;
		[HideInInspector] public long totalGold=0;

		public bool play {
			get {
				return _play;
			}
			set {
				_play = value;
			}
		}

		public float GetSpeedInMin() {
			return Mathf.Abs(speed / 60f);
		}

		public int turns {
			get {
				return _turns;
			}
			private set {
				_turns = value;
			}
		}

		public void Launch(Spinner sp) {
			if (TechManager.onPlay) {
				Logger.Log ("SpinnerRotator: launching spinner " + sp);
				spinner = sp;
				image.sprite = sp.sprite;
				transform.eulerAngles = Vector3.zero;
				lastRot = transform.eulerAngles.z;
				speed = 0f;
				turns = 0;
				recordSpeed = 0f;
				timeRot = 0f;
				totalGold = 0;
				lastGoldAddTime = Time.time;
				lastGoldAddTurns = 0;
				play = true;
			}
		}
		#endregion

		#region Methods
		void Start() {
			play = false;
			maxSpeedSec = maxSpeed * 60f;
		}

		void Update() {
			if (play) {
				if (InputManager.isRotating) {
                    float delta = (InputManager.rotateDir == RotateDirection.Right ? 1f : -1f) * InputManager.magnitude * spinner.speedRatio;
				    //Logger.Log(string.Format("mag: {0}, fps: {1}, dsp: {2}", InputManager.magnitude, 1f / Time.deltaTime, delta));
                    speed += delta;
					if (speed > maxSpeedSec && maxSpeedSec > 0f)
						speed = maxSpeedSec;
					lackTime = 0f;
				} else {
					lackTime += Time.deltaTime;
					if (lackTime > maxLackTime && speed<=0.1f) {
						lackTime = 0f;
						if (onLongLack != null)
							onLongLack ();
					}
				}
				if (Mathf.Abs(speed) > recordSpeed) {
					recordSpeed = Mathf.Abs(speed);
				}
				if (Mathf.Abs(speed) > 0f) {
					transform.Rotate(new Vector3(0, 0, speed*Time.deltaTime));
					speed -= (speed>0 ? 1f : -1f)* spinner.brake*Time.deltaTime*Mathf.Abs(speed);
					if (Mathf.Abs(speed) < 0)
					{
						speed = 0;
					}
				}
				//Logger.Log ("SPEED: " + speed + ",  ANGLE: " + transform.eulerAngles.z + ",  LASTROT: " + lastRot);
				if ((speed > 0 && transform.eulerAngles.z < lastRot)
					|| (speed < 0 && transform.eulerAngles.z > lastRot)
				) {
					turns++;
					if (Time.time - lastGoldAddTime > goldPeriod) {
						CalcAddGold ();
					}
				}
				lastRot = transform.eulerAngles.z;
				timeRot += Time.deltaTime;
			}
		}

		void CalcAddGold() {
			long currentGold = (long)((turns - lastGoldAddTurns) * spinner.goldRatio);
			if (currentGold > 0 && onGoldAdd != null) {
				onGoldAdd (currentGold);
				totalGold += currentGold;
				lastGoldAddTime = Time.time;
				lastGoldAddTurns = turns;
			}
		}
		#endregion

		#region Events
		public Action onLongLack;
		public Action<long> onGoldAdd;
		#endregion
	}
}