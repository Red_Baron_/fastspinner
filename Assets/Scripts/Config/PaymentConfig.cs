﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastSpinner.Payments {
	[CreateAssetMenu(fileName="PaymentConfig", menuName="FastSpinner/PaymentConfig")]
	public class PaymentConfig : ScriptableObject {
		[SerializeField] List<StoreGood> _goods =  new List<StoreGood> {
			new StoreGood("50kchips"),
			new StoreGood("350kchips"),
			new StoreGood("1300kchips_10azik"),
			new StoreGood("3500kchips_25azik"),
			new StoreGood("17150kchips_60azik"),
			new StoreGood("51500kchips_130azik")
		};

		public static List<StoreGood> goods {
			get {
				return instance._goods;
			}
		}

		static PaymentConfig _instance;
		public static PaymentConfig instance {
			get {
				if (_instance==null) {
					_instance = (PaymentConfig)Resources.Load ("PaymentConfig");
					if (_instance == null) {
						_instance = PaymentConfig.CreateInstance<PaymentConfig> ();
						Logger.LogError ("PaymentConfig: loaded instance from resources is null, created instance");
					} 
				}
				return _instance;
			}
		}
	}

}