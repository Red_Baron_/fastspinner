﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FastSpinner {
	[CreateAssetMenu(fileName="SpinnersConfig", menuName="FastSpinner/SpinnersConfig")]
	public class SpinnersConfig : ScriptableObject {
		#region Unity scene settings
		[SerializeField] List<Spinner> _spinners=new List<Spinner>();
		#if UNITY_EDITOR
		[SerializeField] float modifier=1f;
		#endif
		#endregion

		#region Data
		static List<Spinner> _spinnersData= new List<Spinner>();
		static bool readed=false;
		#endregion

		#region Interface
		public static List<int> GetHave() {
			List<int> result = new List<int> ();
			for (int i = 0; i < spinners.Count; i++) {
				if (spinners [i].have)
					result.Add (spinners [i].id);
			}
			return result;
		}

		public static List<Spinner> spinners {
			get {
				if (!readed) {
					Read ();
				}
				return _spinnersData;
			}
		}

		static SpinnersConfig _instance;
		public static SpinnersConfig instance {
			get {
				if (_instance==null) {
					_instance = (SpinnersConfig)Resources.Load ("SpinnersConfig");
					if (_instance == null) {
						_instance = SpinnersConfig.CreateInstance<SpinnersConfig> ();
						Logger.LogError ("SpinnersConfig: loaded instance from resources is null, created instance");
					} 
				}
				return _instance;
			}
		}
		#endregion

		#region Methods
		static void Read() {
			_spinnersData.Clear ();
			for (int i = 0; i < instance._spinners.Count; i++) {
				Spinner s = new Spinner ();
				s.CopyFrom (instance._spinners [i]);
				_spinnersData.Add (s);
			}
			_spinnersData = (_spinnersData.OrderBy (x => x.price)).ToList();
			readed = true;
		}

		#if UNITY_EDITOR
		public void SwitchHave(bool value) {
			for (int i = 0; i < _spinners.Count; i++) {
				_spinners [i].have = value;
			}
		}

		public void SortPrice() {
			_spinners = (_spinners.OrderBy (x => x.price)).ToList ();
		}

		public void SortSpriteName() {
			_spinners = (_spinners.OrderBy (x => x.spriteNumber)).ToList ();
		}

		public void ModifyGoldRatio() {
			for (int i = 0; i < _spinners.Count; i++) {
				_spinners [i].goldRatio *= modifier;
			}
			modifier = 1f;
		}

		public void ModifySpeedRatio() {
			for (int i = 0; i < _spinners.Count; i++) {
				_spinners [i].speedRatio *= modifier;
			}
			modifier = 1f;
		}

		public void ModifyBrake() {
			for (int i = 0; i < _spinners.Count; i++) {
				_spinners [i].brake *= modifier;
			}
			modifier = 1f;
		}
		#endif
		#endregion
	}

	#if UNITY_EDITOR
	[CustomEditor(typeof(SpinnersConfig))]
	public class SpinnersConfigEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			SpinnersConfig inst = (SpinnersConfig)target;
			if(GUILayout.Button("Unlock all"))
			{
				inst.SwitchHave (true);
			}
			if(GUILayout.Button("Lock all"))
			{
				inst.SwitchHave (false);
			}
			if(GUILayout.Button("Sort price"))
			{
				inst.SortPrice ();
			}
			if(GUILayout.Button("Sort sprite name"))
			{
				inst.SortSpriteName ();
			}
			if(GUILayout.Button("Modify gold ratio"))
			{
				inst.ModifyGoldRatio ();
			}
			if(GUILayout.Button("Modify speed ratio"))
			{
				inst.ModifySpeedRatio ();
			}
			if(GUILayout.Button("Modify brake"))
			{
				inst.ModifyBrake ();
			}
		}
	}
	#endif
}