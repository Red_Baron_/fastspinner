﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastSpinner {
	[CreateAssetMenu(fileName="ServiceConfig", menuName="FastSpinner/ServiceConfog")]
	public class ServiceConfig : ScriptableObject {
		[SerializeField] int _version=1;

		public static int version {
			get {
				return instance._version;
			}
		}

		static ServiceConfig _instance;
		public static ServiceConfig instance {
			get {
				if (_instance==null) {
					_instance = (ServiceConfig)Resources.Load ("ServiceConfig");
					if (_instance == null) {
						_instance = ServiceConfig.CreateInstance<ServiceConfig> ();
						Logger.LogError ("ServiceConfig: loaded instance from resources is null, created instance");
					}
				}
				return _instance;
			}
		}
	}
}