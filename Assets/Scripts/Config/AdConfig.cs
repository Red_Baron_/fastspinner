﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastSpinner {
	[CreateAssetMenu(fileName="AdConfig", menuName="FastSpinner/AdConfig")]
	public class AdConfig : ScriptableObject {
		static AdConfig _instance;
		[Header("Ad identifiers")]
		#pragma warning disable 0618
		#pragma warning disable 0414
		[SerializeField] string appodealAndroidId;
		[SerializeField] string appodealiOSId;
		#pragma warning restore 0618
		#pragma warning restore 0414
		[Space]
		[Header("Ad settings")]
		[SerializeField] int _randomPeriod=300;
		[SerializeField] int _rewardPeriod=3600;
		[SerializeField] long _rewardGold=200;

		public static long rewardGold {
			get {
				return instance._rewardGold;
			}
		}

		public static string appodealId{
			get {
				#if UNITY_IOS
				return instance.appodealiOSId;
				#else
				return instance.appodealAndroidId;
				#endif
			}
		}

		public static int rewardPeriod {
			get {
				return instance._rewardPeriod;
			}
		}

		public static int randomPeriod {
			get {
				return instance._randomPeriod;
			}
		}
			
		public static AdConfig instance {
			get {
				if (_instance==null) {
					_instance = (AdConfig)Resources.Load ("AdConfig");
					if (_instance == null) {
						_instance = AdConfig.CreateInstance<AdConfig> ();
						Logger.LogError ("AdConfig: loaded instance from resources is null, created instance");
					} 
				}
				return _instance;
			}
		}

	}
}