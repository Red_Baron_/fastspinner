﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastSpinner {
	[System.Serializable]
	public class AppBonus {
		public string bundleId=string.Empty;
		public string name=string.Empty;
		public long bonus=5000;
		public Sprite sprite;

		#pragma warning disable 0618
		#pragma warning disable 0414
		[SerializeField] string androidUrl=string.Empty;
		[SerializeField] string iOSUrl=string.Empty;
		#pragma warning restore 0618
		#pragma warning restore 0414

		public string url {
			get {
				#if UNITY_IOS
				return iOSUrl;
				#else
				return androidUrl;
				#endif
			}
		}

		public override string ToString ()
		{
			return string.Format ("[bundleId: {0}, name: {1}, bonus: {2}, url: {3}]", bundleId, name, bonus, url);
		}
	}

	[CreateAssetMenu(fileName="OtherAppConfig", menuName="FastSpinner/OtherAppConfog")]
	public class OtherAppConfig : ScriptableObject {
		[SerializeField] List<AppBonus> _apps=new List<AppBonus>();

		public static List<AppBonus> apps {
			get {
				return instance._apps;
			}
		}

		static OtherAppConfig _instance;
		public static OtherAppConfig instance {
			get {
				if (_instance==null) {
					_instance = (OtherAppConfig)Resources.Load ("OtherAppConfig");
					if (_instance == null) {
						_instance = OtherAppConfig.CreateInstance<OtherAppConfig> ();
						Logger.LogError ("OtherAppConfig: loaded instance from resources is null, created instance");
					}
				}
				return _instance;
			}
		}
	}
}