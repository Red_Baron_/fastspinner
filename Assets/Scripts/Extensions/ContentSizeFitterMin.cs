﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

namespace NLExtensions
{
	[AddComponentMenu("Layout/Content Size Fitter Min", 142)]
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public class ContentSizeFitterMin : UIBehaviour, ILayoutSelfController
	{
		public enum FitMode
		{
			Unconstrained,
			MinSize,
			PreferredSize
		}
		 
		[SerializeField] protected float _minSize=0.0001f;
		public float minSize {get { return _minSize; } set {_minSize = value; SetDirty ();}}

		[SerializeField] protected FitMode m_HorizontalFit = FitMode.Unconstrained;
		public FitMode horizontalFit { get { return m_HorizontalFit; } set { m_HorizontalFit = value; SetDirty(); } }

		[SerializeField] protected FitMode m_VerticalFit = FitMode.Unconstrained;
		public FitMode verticalFit { get { return m_VerticalFit; } set { m_VerticalFit=value; SetDirty(); } }

		[System.NonSerialized] private RectTransform m_Rect;
		private RectTransform rectTransform
		{
			get
			{
				if (m_Rect == null)
					m_Rect = GetComponent<RectTransform>();
				return m_Rect;
			}
		}

		private DrivenRectTransformTracker m_Tracker;

		protected ContentSizeFitterMin()
		{}

		#region Unity Lifetime calls

		protected override void OnEnable()
		{
			base.OnEnable();
			SetDirty();
		}

		protected override void OnDisable()
		{
			m_Tracker.Clear();
			LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
			base.OnDisable();
		}

		#endregion

		protected override void OnRectTransformDimensionsChange()
		{
			SetDirty();
		}

		private void HandleSelfFittingAlongAxis(int axis)
		{
			FitMode fitting = (axis == 0 ? horizontalFit : verticalFit);
			if (fitting == FitMode.Unconstrained)
				return;

			m_Tracker.Add(this, rectTransform, (axis == 0 ? DrivenTransformProperties.SizeDeltaX : DrivenTransformProperties.SizeDeltaY));

			// Set size to min or preferred size
			if (fitting == FitMode.MinSize)
				rectTransform.SetSizeWithCurrentAnchors((RectTransform.Axis)axis, GetMinSizeClamp(m_Rect, axis, minSize));
			else
				rectTransform.SetSizeWithCurrentAnchors((RectTransform.Axis)axis, GetPrefferedSizeClamp(m_Rect, axis, minSize));
		}

		public virtual float GetMinSizeClamp(RectTransform rect, int axis, float min) {
			float result = LayoutUtility.GetMinSize (rect, axis);
			return result >= min ? result : min;
		}

		public virtual float GetPrefferedSizeClamp(RectTransform rect, int axis, float min) {
			float result = LayoutUtility.GetPreferredSize (rect, axis);
			return result >= min ? result : min;
		}

		public virtual void SetLayoutHorizontal()
		{
			m_Tracker.Clear();
			HandleSelfFittingAlongAxis(0);
		}

		public virtual void SetLayoutVertical()
		{
			HandleSelfFittingAlongAxis(1);
		}

		protected void SetDirty()
		{
			if (!IsActive())
				return;

			LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
		}

		#if UNITY_EDITOR
		protected override void OnValidate()
		{
			SetDirty();
		}

		#endif
	}
}