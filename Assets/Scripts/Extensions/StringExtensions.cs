﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;
using System;
using System.Text.RegularExpressions;

namespace NLExtensions {
	public static class StringExtensions{

		public enum TimeType {hour, day, month, year};

		public static bool IsNullOrWhitespace(this string s)
		{
			return string.IsNullOrEmpty(s) || s.Trim().Length == 0;
		}

		public static string ReplaceCaseInsensitive(string input, string search, string replacement){
			string result = Regex.Replace(
				input,
				Regex.Escape(search), 
				replacement.Replace("$","$$"), 
				RegexOptions.IgnoreCase
			);
			return result;
		}

		public static string IntToStr (int count) {
			string result = count.ToString ("n", new CultureInfo ("nb-NO"));
			int start = result.IndexOf (",");
			return result.Remove (start, 3);
		}

		public static string LongToStrUniversal(long count, long max) {
			return (count <= max ? LongToStr (count) : LongToStrFormat (count));
		}

		public static string LongToStr (long count) {
			string result = count.ToString ("n", new CultureInfo ("nb-NO"));
			int start = result.IndexOf (",");
			return result.Remove (start, 3);
		}

		public static string CropString (string str, int maxLength) {
			if (str.Length > maxLength) {
				str = str.Remove (maxLength);
			}
			return str;
		}

		public static string IntToStrFormat(int n, bool includeB=false) {
			if (n < 1000)
				return n.ToString();

			if (n < 10000)
				return String.Format("{0:#,.##}K", n - 5);

			if (n < 100000)
				return String.Format("{0:#,.#}K", n - 50);

			if (n < 1000000)
				return String.Format("{0:#,.}K", n - 500);

			if (n < 10000000)
				return String.Format("{0:#,,.##}M", n - 5000);

			if (n < 100000000)
				return String.Format("{0:#,,.#}M", n - 50000);

			if (n < 1000000000 || !includeB)
				return String.Format("{0:#,,.}M", n - 500000);

			return String.Format("{0:#,,,.##}B", n - 5000000);
		}

		public static string LongToStrFormat(long num, bool includeB=false) {
			long i = (long)Math.Pow(10, (int)Math.Max(0, Math.Log10(num) - 2));
			num = num / i * i;

			if (num >= 1000000000 && includeB)
				return (num / 1000000000D).ToString("0.##") + "B";
			if (num >= 1000000)
				return (num / 1000000D).ToString("0.##") + "M";
			if (num >= 1000)
				return (num / 1000D).ToString("0.##") + "K";

			return num.ToString("#,0");
		}

		public static int TimeString(TimeType timeType, int count) {
			switch (timeType) {
			case TimeType.day:
				if (count == 1)
					return 164;
				if (count > 1 && count < 5)
					return 165;
				return 166;
			case TimeType.hour:
				if (count == 1)
					return 167;
				if (count > 1 && count < 5)
					return 168;
				return 169;
			case TimeType.month:
				if (count == 1)
					return 170;
				if (count > 1 && count < 5)
					return 172;
				return 171;
			case TimeType.year:
				if (count == 1)
					return 173;
				if (count > 1 && count < 5)
					return 174;
				return 175;
			default:
				return 44;
			}
		}
	}
}
	