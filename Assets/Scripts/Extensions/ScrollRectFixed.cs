﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace NLExtensions {
	public class ScrollRectFixed : ScrollRect
	{

		override protected void LateUpdate()
		{

			base.LateUpdate();

			if (this.horizontalScrollbar)
			{

				this.horizontalScrollbar.size = 0;
			}
		}

		override public void Rebuild(CanvasUpdate executing)
		{

			base.Rebuild(executing);

			if (this.horizontalScrollbar)
			{

				this.horizontalScrollbar.size = 0;
			}
		}
	}
}