﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace NLExtensions {
	public static class IEnumerableExtensions
	{

		public static IEnumerable<t> Randomize<t>(this IEnumerable<t> target)
		{
			Random r = new Random();

			return target.OrderBy(x=>(r.Next()));
		} 

		public static string GetStringValue(this Dictionary<string,object> source, string key) {
			if (source.ContainsKey (key)) {
				object value = source [key];
				if (value != null)
					return value.ToString ();
			}
			return string.Empty;
		}

		public static long GetLongValue(this Dictionary<string,object> source, string key) {
			if (source.ContainsKey(key)) {
				object value = source [key];
				if (value != null) {
					try {
						return Convert.ToInt64(value);
					}
					catch (FormatException) {
						return 0;
					}
				}
			}
			return 0;
		}

		public static int GetIntValue(this Dictionary<string,object> source, string key) {
			if (source.ContainsKey(key)) {
				object value = source [key];
				if (value != null) {
					try {
						return Convert.ToInt32(value);
					}
					catch (FormatException) {
						return 0;
					}
				}
			}
			return 0;
		}

		public static float GetFloatValue(this Dictionary<string,object> source, string key) {
			if (source.ContainsKey(key)) {
				object value = source [key];
				if (value != null) {
					try {
						return ((float)Convert.ToDouble(value));
					}
					catch (FormatException) {
						return 0;
					}
				}
			}
			return 0;
		}

		public static bool GetBooleanValue(this Dictionary<string,object> source, string key) {
			if (source.ContainsKey (key)) {
				object value = source [key];
				if (value != null) {
					string str = value.ToString ();
					if (str == "1") return true;
					if (str == "0") return false;
					return (str.ToLowerInvariant () == "true");
				}
			}
			return false;
		}

	}


}