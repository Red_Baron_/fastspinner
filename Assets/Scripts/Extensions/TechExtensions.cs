﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace NLExtensions {
	public static class TechExtensions{
		public  static bool IsAppInstalled(string bundleId){
			bool success = false;
			#if UNITY_ANDROID && !UNITY_EDITOR
			AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
			AndroidJavaObject launchIntent = null;
			try
			{
				launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage",bundleId);
			}
			catch {}
			up.Dispose();
			ca.Dispose();
			packageManager.Dispose();
			success = (launchIntent != null);
			if (launchIntent!=null)
				launchIntent.Dispose();
			#endif
			return success;
		}
	}
}