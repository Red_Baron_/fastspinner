﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace NLExtensions {
	public static class GraphicExtensions {
		public static float GetWidth(this RectTransform tr) {
			return tr.rect.width;
		}

		public static float GetHeight(this RectTransform tr) {
			return tr.rect.height;
		}

		public static void Switch(this CanvasGroup group, bool value) {
			if (group != null) {
				group.blocksRaycasts = value;
				group.interactable = value;
				if (value)
					group.alpha = 1f;
				else
					group.alpha = 0f;
			}
		}

        public static void Switch(this CanvasGroup group, bool value, float alpha)
        {
			if (group != null) {
				group.blocksRaycasts = value;
				group.interactable = value;
				group.alpha = alpha;
			}
        }
    }

}


