﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Spinner{
	public int id = 0;
	public float speedRatio=1f;
	public float brake=5f;
	public float goldRatio=0.1f;
	public long price=100;
	public Sprite sprite;
	public bool have=false;

	public override string ToString ()
	{
		return string.Format ("[id:{0}, speedRatio:{1}, brake:{2}, goldRatio:{3}, price:{4}, have:{5}]",id, speedRatio, brake, goldRatio, price, have);
	}

	public void CopyFrom(Spinner original) {
		id = original.id;
		speedRatio = original.speedRatio;
		brake = original.brake;
		goldRatio = original.goldRatio;
		price = original.price;
		sprite = original.sprite;
		have = original.have;
	}

	#if UNITY_EDITOR
	public int spriteNumber {
		get {
			int r = 0;
			int z = 0;
			if (int.TryParse (sprite.name, out z))
				r = z;
			return r;
		}
	}
	#endif
}