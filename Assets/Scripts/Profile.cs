﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Profile{
	public long gold=0;
	public List<int> spinners=new List<int>();
	public int selectedSpinner=1;
	public long maxGoldEarned=0;
	public long maxTurns=0;
	public float maxSpeed=0f;
	public float maxTime = 0f;
	public bool hasPurchases=false;
	public List<string> appBonuses=new List<string>();

	public void Reset() {
		gold = 0;
		spinners = new List<int> (){ 1 };
		selectedSpinner = 1;
		maxGoldEarned = 0;
		maxTurns = 0;
		maxSpeed = 0f;
		maxTime = 0f;
		hasPurchases = false;
		appBonuses.Clear ();
	}

	public override string ToString ()
	{
		return string.Format ("[gold:{0}, spinners:{1}, selectedSpinner:{2}, maxGoldEarned:{3}, maxTurns:{4}, maxSpeed:{5}, hasPurchases:{6}, maxTime:{7}]",
			gold, ListToStr(spinners), selectedSpinner, maxGoldEarned, maxTurns, maxSpeed, hasPurchases, maxTime
		);
	}

	string ListToStr(List<int> list) {
		string str = string.Empty;
		if (list != null) {
			str += "[";
			for (int i = 0; i < list.Count; i++) {
				str += list [i].ToString ();
				if (i < list.Count - 1)
					str += ",";
			}
			str += "]";
		}
		return str;
	}
}