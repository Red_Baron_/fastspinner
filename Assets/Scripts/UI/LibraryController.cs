﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FastSpinner.Scroll;
using SimpleLocalizator;
using NLExtensions;

namespace FastSpinner {
	public class LibraryController : MonoBehaviour {
		[SerializeField] Text speedText;
		[SerializeField] Text brakeText;
		[SerializeField] Text goldText;
		[SerializeField] PageScroller pageScroller;
		[SerializeField] GameObject prefab;
		[SerializeField] Transform parent;
		[SerializeField] Text selectText;
		[SerializeField] GameObject buyIcon;
		[SerializeField] Button selectButton;
		[SerializeField] GameObject leftArrow;
		[SerializeField] GameObject rightArrow;
		[SerializeField] Text openText;

		int _index = 0;
		int index {
			get {
				return _index;
			}
			set {
				_index = value;
				Refresh ();
			}
		}

		void Start() {
			GameObject spawned;
			for (int i = 0; i < SpinnersConfig.spinners.Count; i++) {
				spawned = Instantiate (prefab);
				spawned.transform.SetParent (parent, false);
				spawned.transform.localScale = Vector3.one;
				spawned.GetComponent<SpinnerItem> ().spinner = SpinnersConfig.spinners [i];
			}
			pageScroller.Refresh ();
			index = 0;
		}

		void OnEnable() {
			InputManager.onSwipe += OnSwipe;
		}

		void OnDisable() {
			InputManager.onSwipe -= OnSwipe;
		}

		void OnSwipe(SwipeDirection dir) {
			if (dir == SwipeDirection.Right)
				Right ();
			else if (dir == SwipeDirection.Left)
				Left ();
		}

		public void Select() {
			if (SpinnersConfig.spinners [index].have) {
				CoreManager.SelectSpinner (SpinnersConfig.spinners [index].id);
				Refresh ();
			} else {
				if (CoreManager.gold >= SpinnersConfig.spinners [index].price) {
					CoreManager.BuySpinner (index);
					SpinnersConfig.spinners [index].have = true;
					Refresh ();
				}
			}
		}

		public void Refresh() {
			speedText.text = SpinnersConfig.spinners [index].speedRatio.ToString();
			brakeText.text = SpinnersConfig.spinners [index].brake.ToString();
			goldText.text = SpinnersConfig.spinners [index].goldRatio.ToString();
			buyIcon.SetActive (!SpinnersConfig.spinners [index].have);
			string str = string.Empty;
			bool selected = false;
			if (SpinnersConfig.spinners [index].have) {
				selected = (SpinnersConfig.spinners [index].id == CoreManager.selectSpinner.id);
				if (!selected)
					str = LanguageManager.GetString (8).ToUpper ();
				else
					str = LanguageManager.GetString (12).ToUpper ();
			}
			else str = LanguageManager.GetString(9).ToUpper()+" "+NLExtensions.StringExtensions.LongToStrUniversal(SpinnersConfig.spinners [index].price, 999999);
			selectText.text = str;
			selectButton.interactable = (!selected);
			leftArrow.SetActive (index > 0);
			rightArrow.SetActive (index < SpinnersConfig.spinners.Count - 1);
			openText.text = LanguageManager.GetString (37) +" "+ OpenCount().ToString ()+" / "+SpinnersConfig.spinners.Count;
		}

		int OpenCount() {
			int result = 0;
			for (int i = 0; i < SpinnersConfig.spinners.Count; i++) {
				if (SpinnersConfig.spinners [i].have)
					result++;
			}
			return result;
		}

		public void Right() {
			if (!pageScroller.isMoving) {
				if (index < SpinnersConfig.spinners.Count - 1) {
					index++;
					pageScroller.MoveRight (null);
				}
			} 
		}

		public void Left() {
			if (!pageScroller.isMoving) {
				if (index > 0) {
					index--;
					pageScroller.MoveLeft (null);
				} 
			}
		}
	}
}