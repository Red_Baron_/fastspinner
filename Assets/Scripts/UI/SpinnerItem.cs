﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpinnerItem : MonoBehaviour {
	[SerializeField] Image image;

	Spinner _spinner;
	public Spinner spinner {
		get {
			return _spinner;
		}
		set {
			_spinner = value;
			Refresh ();
		}
	}

	void Refresh() {
		if (spinner != null) {
			image.sprite = spinner.sprite;
		}
	}
}
