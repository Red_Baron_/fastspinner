﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FastSpinner {
	public class AnimButton : MonoBehaviour {
		[SerializeField] Animator animator;
		[SerializeField] Button button;
		[SerializeField] string paramName="Anim";
		[SerializeField] float delay=0f;

		bool _enable=false;
		public bool enable {
			get {
				return _enable;
			}
			set {
				_enable = value;
				button.interactable = value;
				StartCoroutine (SetParamDelay (value));
				//animator.SetBool (paramName, value);
			}
		}

		IEnumerator SetParamDelay(bool value) {
			if (value) yield return delay;
			animator.SetBool (paramName, value);
		}

		void OnValidate() {
			enable = enable;
		}
	}
}