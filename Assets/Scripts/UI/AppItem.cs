﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FastSpinner {
	public class AppItem : MonoBehaviour {
		[SerializeField] Image iconImage;
		[SerializeField] Text text;

		[SerializeField] AppBonus _bonus= new AppBonus();
		public AppBonus bonus {
			get {
				return _bonus;
			}
			set {
				_bonus = value;
			}
		}


		void Refresh() {
			iconImage.sprite = bonus.sprite;
			text.text = bonus.name;
		}

		public void OnClick() {
			Logger.Log ("AppItem: opening app url "+bonus);
			Application.OpenURL (bonus.url);
		}
	}
}