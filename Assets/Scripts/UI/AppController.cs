﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FastSpinner {
	public class AppController : MonoBehaviour {
		[SerializeField] GameObject prefab;
		[SerializeField] Transform parent;

		List<GameObject> spawned = new List<GameObject> ();

		public void Refresh(List<AppBonus> apps) {
			for (int i = 0; i < spawned.Count; i++) {
				Destroy (spawned [i]);
			}
			spawned.Clear ();
			GameObject g;
			for (int i = 0; i < apps.Count; i++) {
				g = Instantiate (prefab);
				g.transform.SetParent (parent);
				g.transform.localScale = Vector3.one;
				spawned.Add (g);
				g.GetComponent<AppItem>().bonus=apps[i];
			}
		}
	}
}