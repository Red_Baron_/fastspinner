﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using UnityEngine;
using System;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

namespace FastSpinner {
	public class AdManager : MonoBehaviour, IRewardedVideoAdListener, IInterstitialAdListener, IBannerAdListener{
		#region Unity scene settings
		[SerializeField] bool test=false;
		#endregion

		#region Data
		static AdManager _instance;
		static bool _rewardReady=false;

		static float lastRewardAd = 0f;
		static float lastRandomAd=0f;

		static AdManager instance {
			get {
				return _instance;
			}
			set {
				_instance = value;
			}
		}

		static bool seeAd {
			get {
				return CoreManager.seeAd;
			}
		}
		#endregion

		#region Interface
		public static bool rewardReady {
			get {
				return _rewardReady;
			}
			private set {
				if (value != _rewardReady) {
					_rewardReady = value;
					if (onRewardStatusChanged != null)
						onRewardStatusChanged (value);
				}
			}
		}

		public static void ShowRewardVideo() {
			if (isRewardReady()) {
				if (TechManager.isEditor) {
					Logger.Log ("AdManager: closing reward ad on standalone.");
					SuccessReward (false);
					return;
				} else {
					if (Appodeal.isLoaded (Appodeal.REWARDED_VIDEO)) {
						Logger.Log ("AdManager: showing AdMob reward ad.");
						Appodeal.show (Appodeal.REWARDED_VIDEO);
					}
				}
			}
		}

		static bool isRewardReady() {
			bool timeReady=(Time.time - lastRewardAd >= AdConfig.rewardPeriod);
			bool appodealReady=TechManager.isEditor || (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO));
			return  (timeReady && appodealReady);
		}
		#endregion

		#region Methods
		IEnumerator CheckReward() {
			var t = new WaitForSeconds (1f);
			while (true) {
				rewardReady = isRewardReady ();
				yield return t;
			}
		}

		void Start () {
			if (instance == null) {
				instance = this;
				lastRandomAd = -AdConfig.randomPeriod*2f;
				lastRewardAd = -AdConfig.rewardPeriod*2f;
					ViewManager.onSwitchScreen += OnSwitchScreenState;
				if (!TechManager.isEditor) {
					Appodeal.initialize (AdConfig.appodealId, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO | Appodeal.BANNER);
					Appodeal.setTesting (test);
					Appodeal.disableNetwork ("unity_ads");
					Appodeal.setRewardedVideoCallbacks (this);
					Appodeal.setInterstitialCallbacks (this);
				}
				Logger.Log ("AdManager: initialized. Test: "+test+".");
				StartCoroutine (CheckReward ());
			} else if (instance != this)
				Destroy (this);
		}

		void OnDisable() {
			ViewManager.onSwitchScreen -= OnSwitchScreenState;
		}

		void OnSwitchScreenState(State st) {
			if (st != State.play && st != State.shop && !TechManager.isEditor) {
				if (Time.time - lastRandomAd >= AdConfig.randomPeriod || (ViewManager.instance.oldState==State.play && ViewManager.instance.state==State.pause)) {
                    ShowRandom();
				}
			}
		}

	    void ShowRandom()
	    {
			if (!Application.isEditor && Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
				Appodeal.show (Appodeal.INTERSTITIAL);
			}
        }

		static void SuccessReward(bool click) {
			Logger.Log ("AdManager: SuccessReward: click:"+click);
			if (onSuccessReward != null)
				onSuccessReward (click);
		}

		IEnumerator DelayCor(Action action, float delay) {
			yield return new WaitForSeconds (delay);
			if (action != null)
				action ();
		}

		static void Delay(Action action, float delay) {
			instance.StartCoroutine(instance.DelayCor(action, delay));
		}

		void OnApplicationFocus(bool hasFocus) {
			if(hasFocus) {
				Appodeal.onResume();
			}
		}
		#endregion

		#region Callbacks
		public void onBannerLoaded(bool precache) { Logger.Log("AdManager: banner loaded"); Appodeal.show (Appodeal.BANNER_TOP);}
		public void onBannerFailedToLoad() { Logger.Log("AdManager: banner failed"); }
		public void onBannerShown() { 
			Logger.Log("AdManager: banner opened"); 
		}
		public void onBannerClicked() { 
			Logger.Log("AdManager: banner clicked"); 
			if (Appodeal.isLoaded(Appodeal.BANNER)) Appodeal.show (Appodeal.BANNER_TOP);
		}

		public void onInterstitialLoaded(bool isPrecache) { Logger.Log("AdManager: Interstitial loaded"); }
		public void onInterstitialFailedToLoad() { Logger.Log("AdManager: Interstitial failed"); }
		public void onInterstitialShown() { Logger.Log("AdManager: Interstitial opened"); }
		public void onInterstitialClosed() { 
			Logger.Log("AdManager: Interstitial closed"); 
			lastRandomAd = Time.time;
		}
		public void onInterstitialClicked() { 
			Logger.Log("AdManager: Interstitial clicked"); 
			lastRandomAd = Time.time;
		}

		public void onRewardedVideoLoaded() { Logger.Log("AdManager: Video loaded"); }
		public void onRewardedVideoFailedToLoad() { Logger.Log("AdManager: Video failed"); }
		public void onRewardedVideoShown() { Logger.Log("AdManager: Video shown"); }
		public void onRewardedVideoClosed(bool finished) { 
			Logger.Log("AdManager: Video closed"); 
			lastRewardAd = Time.time;
		}
		public void onRewardedVideoFinished(int amount, string name) { 
			Logger.Log("AdManager: Reward: " + amount + " " + name); 
			SuccessReward (false);
			lastRewardAd = Time.time;
		}
		#endregion

		#region Events
		public static Action<bool> onSuccessReward;
		public static Action<bool> onRewardStatusChanged;
		#endregion
	}
}