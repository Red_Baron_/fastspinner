﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace FastSpinner {
	public class CoreManager : MonoBehaviour {
		#region Data
		const string profileKey = "profile1";
		static Profile _profile = new Profile();
		#endregion

		#region Interface
		public static bool seeAd {
			get {
				return true;
			}
		}

		public static Spinner selectSpinner {
			get {
				var s = SpinnersConfig.spinners.Find (x => x.id == profile.selectedSpinner);
				if (s == null) {
					for (int i = 0; i < SpinnersConfig.spinners.Count; i++) {
						if (profile.spinners.Contains(SpinnersConfig.spinners[i].id)) {
							SelectSpinner(i);
							break;
						}
					}
				}
				return s;
			}
		}

		public static Profile profile {
			get {
				return _profile;
			}
			private set {
				_profile = value;
			}
		}

		public static long gold {
			get {
				return profile.gold;
			}
			set {
				if (value >= 0) {
					profile.gold = value;
					WriteSave ();
					if (onProfileUpdate != null)
						onProfileUpdate ();
				}
			}
		}

		public static void AddRewardAd(bool click) {
			gold += AdConfig.rewardGold*(click ? 2: 1);
		}

		public static void BuyGold(long addGold) {
			profile.hasPurchases = true;
			gold += addGold;
		}

		public static void SelectSpinner(int id) {
			var s = SpinnersConfig.spinners.Find (x => x.id == id);
			if (s!=null) {
				Logger.Log ("CoreManager: selecting active spinner " + s);
				profile.selectedSpinner = id;
				WriteSave ();
				if (onProfileUpdate != null)
					onProfileUpdate ();
			}
			else {
				Logger.Log ("CoreManager: not contains spinner with id " + id);
			}
		}

		public static void BuySpinner(int index) {
			if (!SpinnersConfig.spinners [index].have && gold >= SpinnersConfig.spinners [index].price) {
				Logger.Log ("CoreManager: buying spinner " + SpinnersConfig.spinners [index]);
				if (!profile.spinners.Contains(SpinnersConfig.spinners [index].id))
					profile.spinners.Add (SpinnersConfig.spinners [index].id);
				gold -= SpinnersConfig.spinners [index].price;
			}
		}
		public static void AddAppBonus(AppBonus bonus) {
			Logger.Log ("CoreManager: adding app bonus " + bonus);
			if (!profile.appBonuses.Contains (bonus.bundleId)) {
				profile.appBonuses.Add (bonus.bundleId);
				gold += bonus.bonus;
				WriteSave ();
			}
		}

		public static void ResetProgress() {
			Logger.Log ("CoreManager: reset progress.");
			profile.Reset ();
			WriteSave ();
			if (onProfileUpdate != null)
				onProfileUpdate ();
		}
		#endregion

		#region Methods
		void OnEnable() {
			Logger.Log ("CoreManager: initialized.");
			profile.spinners = SpinnersConfig.GetHave ();
			ReadSave ();
		}

		static void ReadSave() {
			string str = PlayerPrefs.GetString ("profile1", "null");
			if (!str.Equals ("null")) {
				var p = JsonUtility.FromJson<Profile> (str);
				if (p != null) {
					profile = p;
					UpdateItems ();
					Logger.Log ("CoreManager: reading save: " + str+", profile: "+profile);
					if (onProfileUpdate != null)
						onProfileUpdate ();
				}
			} else {
				UpdateItems ();
				WriteSave ();
			}
		}

		static void UpdateItems() {
			for (int i = 0; i < SpinnersConfig.spinners.Count; i++) {
				if (!profile.spinners.Contains (SpinnersConfig.spinners [i].id)) {
					if (SpinnersConfig.spinners [i].price == 0)
						SpinnersConfig.spinners [i].have = true;
					if (SpinnersConfig.spinners [i].have) {
						profile.spinners.Add (SpinnersConfig.spinners [i].id);
					}
				} else {
					SpinnersConfig.spinners [i].have = true;
				}
			}
		}

		public static void WriteSave() {
			string str = JsonUtility.ToJson (profile);
			Logger.Log ("CoreManager: writing save: "+str);
			PlayerPrefs.SetString (profileKey, str);
		}

		void OnApplicationQuit() {
			WriteSave ();
		}
		#endregion

		#region Events
		public static Action onProfileUpdate;
		#endregion
	}
}