﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SimpleLocalizator;

namespace FastSpinner {
	public class TechManager : MonoBehaviour {
		#region Unity scene settings
		[SerializeField] bool setFps=false;
		[SerializeField] int fps=30;
		#endregion

		#region Data
		static bool _onPlay = false;
		static bool _isEditor=false;
		static bool _firstLaunchAfterUpdate = false;
		static bool _firstLaunch=false;
		static Platform _platform=Platform.Windows;
		static TechManager instance;
		static bool firstFocus=false;
		static bool firstPause=false;
		#endregion

		#region Interface
		public static bool firstLaunchAfterUpdate {
			get {
				return _firstLaunchAfterUpdate;
			}
			private set {
				_firstLaunchAfterUpdate = value;
			}
		}

		public static bool firstLaunch {
			get {
				return _firstLaunch;
			}
			private set {
				_firstLaunch = value;
			}
		}

		public static bool onPlay {
			get {
				return _onPlay;
			}
			private set {
				_onPlay = value;
			}
		}

		public static bool isEditor {
			get {
				return _isEditor;
			}
			private set {
				_isEditor = value;
			}
		}

		public static string GetTimeData() {
			return DateTime.Now.ToString ("yyyy/MM/dd HH:mm:ss");
		}

		public static Platform platform {
			get {
				return _platform;
			}
			private set {
				_platform = value;
			}
		}

		public static RuntimePlatform device{
			get {
				return Application.platform;
			}
		}
		#endregion

		#region Events
		public static Action<bool> onPause;
		public static Action<bool> onFocus;
		#endregion

		#region Methods
		void OnApplicationFocus(bool hasFocus) {
			//Logger.Log ("TechManager: OnApplicationFocus "+hasFocus);
			if (firstFocus && onFocus!=null) {
				onFocus (hasFocus);
			}
			firstFocus=true;
		}

		void OnApplicationPause(bool pause) {
			//Logger.Log ("TechManager: OnApplicationPause "+pause);
			if (firstPause && onPause!=null) {
				onPause (pause);
			}
			firstPause=true;
		}

		void Awake() {
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			if (instance == null) {
				instance = this;
				if (setFps)
					Application.targetFrameRate = fps;
				ApplyQuality ();
				#if UNITY_EDITOR
				if (Application.isEditor)
					isEditor = true;
				#endif
				#if UNITY_ANDROID
				platform = Platform.Android;
				#elif UNITY_IOS
				platform = Platform.iOS;
				#elif UNITY_STANDALONE_WIN
				platform = Platform.Windows;
				#endif
				onPlay = true;
				int ver = PlayerPrefs.GetInt ("last_launch_version", 0);
				firstLaunchAfterUpdate = ver != ServiceConfig.version;
				firstLaunch = (ver == 0);
				PlayerPrefs.SetInt ("last_launch_version", ServiceConfig.version);
				Logger.Log(string.Format("TechManager: initialized. Time: {0}, platform: {1}, version: {2}, frm:{3}, lang:{4}, system memory:{5}, graphics memory:{6}. quality:{7}, deviceId:{8}, firstLaunch:{9}, firstLaunchAfterUpdate:{10}",
					GetTimeData(),
					Application.platform, 			
					ServiceConfig.version,
					Application.targetFrameRate,
					LanguageManager.currentLanguage,
					SystemInfo.systemMemorySize,
					SystemInfo.graphicsMemorySize,
					QualitySettings.GetQualityLevel(),
					SystemInfo.deviceUniqueIdentifier,
					firstLaunch,
					firstLaunchAfterUpdate
				)
				);
			}
			else if (instance!=this)
				Destroy (this);
		}

		void ApplyQuality() {
			if (SystemInfo.systemMemorySize < 900) {
				Logger.Log ("TechManager: set quality level to 1.");
				QualitySettings.SetQualityLevel (1);
			}
		}
		#endregion
	}
}