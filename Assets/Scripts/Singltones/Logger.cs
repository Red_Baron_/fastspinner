﻿/*Copyright (C) GameStudio316
*/
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;
using NLExtensions;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FastSpinner {
	public class Logger : MonoBehaviour {
		#region Data
		static bool addStackTrace = false;
		#if UNITY_EDITOR
		static bool init = false;
		#endif
		static Logger _instance;
		static Logger instance {
			get {
				return _instance;
			}
		}
		StreamWriter writer;
		string oldFileName=string.Empty;
		#if !UNITY_IOS
		static int stringsCount=0;
		#endif
		static int number=1;
		static bool diskFull = false;

		static string _persistentDataPath = string.Empty;
		static string logDirectory {
			get {
				if (_persistentDataPath.Length == 0)
					#if UNITY_STANDALONE_LINUX
					_persistentDataPath=Application.dataPath;
					#else
					_persistentDataPath = Application.persistentDataPath;
					#endif
				return Path.Combine (_persistentDataPath, "Logs");
			}
		}

		static string logFileName{
			get {
				string subName = "log_";
				return Path.Combine (logDirectory, ValidateFileName(subName+GetUniqueTimeData()+"_"+(number==1 ? string.Empty : number.ToString())+".txt"));
			}
		}
		#endregion

		#region Methods
		static string ValidateFileName(string badFileName)
		{
			var allChars= badFileName.ToCharArray();
			var invalidChars=Path.GetInvalidFileNameChars();
			var validChars=new List<char>(allChars.Length);
			var lbound = invalidChars.GetLowerBound (0);
				foreach(Char ch in allChars)
					if(Array.IndexOf( invalidChars,ch) < lbound)
						validChars.Add(ch);
				return new String( validChars.ToArray());
		}

		static string[] GetLogFilesByDate() {
			DirectoryInfo info = new DirectoryInfo(logDirectory);
			FileInfo[] files = info.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();
			string[] result = new string[files.Length];
			for (int i = 0; i < result.Length; i++) {
				result[i] = files [i].FullName;
			}
			return result;
		}

		void RenameLogFile(string addString) {
			addString = ValidateFileName (addString);
			Directory.CreateDirectory (logDirectory);
			writer.Close ();
			writer.Dispose ();
			string newName=oldFileName.Replace(".txt", addString + ".txt");
			try {
				File.Move(oldFileName, newName);
			}
			catch{
			};
			writer = new StreamWriter (newName, true, System.Text.Encoding.UTF8);
			writer.AutoFlush = true;
		}

		bool CanDeleteLog (string file) {
			return DateTime.Now.Subtract (File.GetCreationTime (file)).Days > 3;
		}

		void OnEnable() {
			#if UNITY_EDITOR
			init = true;
			#else
			Application.logMessageReceived += LogMessage;
			#endif
			if (_instance == null || _instance==this) {
				_instance = this;
				if (writer == null) {
					CreateWriter ();
				}
				try {
					writer.WriteLine("start");
					#if UNITY_EDITOR
					writer.WriteLine("editor");
					#endif
				}
				catch {
					diskFull = true;
				}
				//Debug.Log ("Logger: initialized.");
			} else  {
				//Debug.LogError ("Logger: more than one instance!");
				Destroy (this);
			}
		}

		void CreateWriter(bool append=false) {
			Directory.CreateDirectory (logDirectory);
			if (!append) {
				ClearAllLogs ();
			}
			oldFileName = logFileName;
			writer = new StreamWriter (oldFileName, append, System.Text.Encoding.UTF8);
			writer.AutoFlush = true;
			#if !UNITY_IOS
			stringsCount = 0;
			#endif
		}

		void OnDisable() {
			#if !UNITY_EDITOR
			Application.logMessageReceived -= LogMessage;
			#endif
		}

		void OnDestroy() {
			if (writer != null) {
				writer.Close ();
				writer.Dispose ();
				writer = null;
			}
		}

		void ClearAllLogs() {
			string[] logs = Directory.GetFiles (logDirectory);
			for (int i = 0; i < logs.Length; i++) {
				if (logs [i].Contains ("log") && CanDeleteLog(logs[i])) {
					try {
						File.Delete(logs[i]);
					}
					catch {
					}
				}
			}
		}

		void Write(string str) {
			if (!diskFull) {
				#if !UNITY_IOS
				if (writer == null)
					CreateWriter ();
				writer.WriteLine (str);
				stringsCount++;
				if (stringsCount+1	 > 100000) {
					number++;
					stringsCount = 0;
					if (instance.writer != null) {
						instance.writer.Close ();
						instance.writer.Dispose ();
						instance.writer = null;
					}
					if (!diskFull)
						CreateWriter ();
				}
				#else
				Debug.Log(str);
				#endif
			}
		}

		static string GetUniqueTimeData () {
			string result = TechManager.GetTimeData ();
			result = result.Replace (":", "_");
			result = result.Replace ("\\", "_");
			result = result.Replace ("/", "_");
			return result.Replace (" ", "_");
		}
		#endregion

		#region Interface
		public static void Log(string log) {
			if (instance!=null) {
				log = TechManager.GetTimeData()+" "+log;
			}
			#if UNITY_EDITOR
			if (init)
				Debug.Log (log);
			if ((instance!=null) && (TechManager.isEditor || EditorApplication.isPlaying))
				instance.Write(log);
			#else
			if (instance!=null)
			instance.Write(log);
			#endif
		}

		public static void LogError(string log) {
			if (instance!=null) {
				log = TechManager.GetTimeData()+" "+log;
			}
			#if UNITY_EDITOR
			if (init)
				Debug.LogError (log);
			#endif
			if (instance!=null)
				instance.Write("Error:"+log);
		}

		public void LogMessage(string message, string stackTrace, LogType type)
		{
			if (!message.Contains("host id out of bound id")) {
				string str = message+((stackTrace.IsNullOrWhitespace() || !addStackTrace) ? string.Empty : ", stacktrace: "+stackTrace)+".";
				if (type == LogType.Error)
					Debug.LogError (str);
				else
					Debug.Log (str);
			}
		}
		#endregion
	}
}