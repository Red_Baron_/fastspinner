﻿/*Copyright (C) GameStudio316
*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NLExtensions;
using System;
using SimpleLocalizator;
using FastSpinner.Payments;
using Sharing;

namespace FastSpinner {
	public class ViewManager : MonoBehaviour {
		#region Unity scene settings
		[SerializeField] SpinnerRotator rotator;
		[SerializeField] LibraryController library;
		[SerializeField] CanvasGroup playScreen=null;
		[SerializeField] CanvasGroup mainScreen=null;
		[SerializeField] CanvasGroup spinnersScreen=null;
		[SerializeField] CanvasGroup shopScreen=null;
		[SerializeField] CanvasGroup notifyScreen=null;
		[SerializeField] CanvasGroup pauseScreen=null;
		[SerializeField] CanvasGroup finishPlayScreen=null;
		[SerializeField] CanvasGroup backgroundScreen=null;
		[SerializeField] CanvasGroup otherAppsScreen=null;
		[SerializeField] CanvasGroup settingsScreen=null;
		[SerializeField] Text goldText=null;
		[SerializeField] GameObject backButton=null;
		[SerializeField] AnimButton watchAdButton;
		[SerializeField] GameObject otherAppsButton;
		[SerializeField] AppController appController;
		[SerializeField] Text notifyText;
		[SerializeField] Text speedText;
		[SerializeField] Text turnsText;
		[SerializeField] Text finishRecordText;
		[SerializeField] Text finishTimeText;
		[SerializeField] Text finishSpeedText;
		[SerializeField] Text finishTurnsText;
		[SerializeField] Text finishGoldText;
		[SerializeField] Color stdColor;
		[SerializeField] Color recordColor;
		[SerializeField] Animator[] addGoldAnims;
		[SerializeField] Text addGoldText;
		[SerializeField] Text recordGoldText;
		[SerializeField] Text recordTurnsText;
		[SerializeField] Text recordSpeedText;
		[SerializeField] Text recordTimeText;
		[SerializeField] Text versionText;
		#endregion

		#region Data
		[SerializeField] State _state = State.main;
		State _oldState=State.main;
		static ViewManager _instance;

		public State oldState {
			get {
				return _oldState;
			}
			private set {
				_oldState = value;
			}
		}

		List<string> notifyQueue = new List<string> ();

		public State state {
			get {
				return _state;
			}
			private set {
				SwitchOffAllCanvasGroups ();
				if ((value == State.main || value == State.shop || value == State.spinners || value == State.otherApps) && notifyQueue.Count > 0) {
					NextNotify ();
					return;
				}
				if (value == oldState) {
					switch (state) {
					case State.shop:
						value = State.main;
						break;
					}
				}
				GetScreen (_state).Switch (false);
				if (oldState != value && _state!=State.notify && _state!=State.pause)
					oldState = _state;
				_state = value;
				GetScreen (_state).Switch (true);
				switch (state) {
				case State.main:
					break;
				case State.play:
					break;
				case State.spinners:
					library.Refresh ();
					break;
				case State.shop:
					break;
				case State.otherApps:
					break;
				case State.settings:
					break;
				case State.notify:
					if (oldState!=state) GetScreen (oldState).alpha = 0.6f;
					backgroundScreen.alpha = 0.6f;
					backgroundScreen.interactable = false;
					break;
				case State.pause:
					if (oldState!=state) GetScreen (oldState).alpha = 0.6f;
					backgroundScreen.alpha = 0.6f;
					backgroundScreen.interactable = false;
					break;
				case State.finishPlay:
					if (oldState != state)
						GetScreen (oldState).alpha = 0.6f;
					backgroundScreen.alpha = 0.6f;
					backgroundScreen.interactable = false;
					ShowPlayResult ();
					break;
				}
				if (state != State.notify && state != State.pause && state!=State.finishPlay) {
					backgroundScreen.alpha = 1f;
					backgroundScreen.interactable = true;
				}
				backButton.SetActive (state != State.main);
				if (onSwitchScreen != null)
					onSwitchScreen (state);
			}
		}
		#endregion

		#region Interface
		public static ViewManager instance {
			get {
				return _instance;
			}
			private set {
				_instance = value;
			}
		}

	    public void Share(int id)
	    {
	        if (TechManager.platform != Platform.Windows)
	        {
	            SharingManager.ShareVia((ShareApp)id, LanguageManager.GetString(40), GetAppUrl());
            }
	    }

		public void Settings() {
			state = State.settings;
		}

		public void ResetProgress() {
			state = State.main;
			oldState = State.main;
			CoreManager.ResetProgress ();
			AchievmentManager.Refresh ();
		}

		public void OtherApps() {
			state = State.otherApps;
		}

		public void OkNotify() {
			//state = oldState;
			NextNotify();
		}

		public void Shop() {
			state = State.shop;
		}

		public void Spin() {
			state = State.play;
			rotator.Launch (CoreManager.selectSpinner);
		}

		public void MySpinners() {
			state = State.spinners;
		}

		public void WatchAd() {
			AdManager.ShowRewardVideo ();
			watchAdButton.enable = AdManager.rewardReady;
		}

		public void OnReturn() {
			switch (state) {
			case State.pause:
				ResumePlay ();
				break;
			case State.play:
				rotator.play = false;
				state = State.pause;
				break;
			case State.spinners:
				state = oldState;
				break;
			case State.shop:
				state = oldState;
				break;
			case State.otherApps:
				state = oldState;
				break;
			case State.settings:
				state = oldState;
				break;
			case State.notify:
				OkNotify ();
				break;
			}
		}

		public void Buy(string id) {
			PurchaseManager.Buy (id);
		}

		public void ResumePlay() {
			state = State.play;
			rotator.play = true;
		}

		public void FinishPlay() {
			rotator.play = false;
			if (rotator.timeRot > 5f || rotator.totalGold > 0) {
				state = State.finishPlay;
			} else
				state = State.main;
		}

		public void OkPlay() {
			state = State.main;
		}
        #endregion

        #region Methods
	    static string GetAppUrl()
	    {
	        switch (TechManager.platform)
	        {
	            case Platform.Android:
	                return "https://play.google.com/store/apps/details?id=com.gamestudio316.fastspinner";
	            case Platform.iOS:
	                return "https://itunes.apple.com/us/app/fast-spinner/id1269964631";
	            default:
	                return "https://play.google.com/store/apps/details?id=com.gamestudio316.fastspinner";
	        }
	    }

        void OnAddPlayGold(long gold) {
			for (int i = 0; i < addGoldAnims.Length; i++) {
				addGoldAnims[i].SetTrigger ("Anim");
			}
			addGoldText.text = "+ "+gold.ToString ();
			CoreManager.gold += gold;
		}

		void ShowPlayResult() {
			bool record = (rotator.recordSpeed/60f > CoreManager.profile.maxSpeed || rotator.turns > CoreManager.profile.maxTurns || rotator.timeRot>CoreManager.profile.maxTime || rotator.totalGold>CoreManager.profile.maxGoldEarned);
			finishRecordText.gameObject.SetActive (record); 
			finishRecordText.text = LanguageManager.GetString (record ? 28 : 10);
			finishTimeText.text = LanguageManager.GetString (29) + ": " + ((int)(rotator.timeRot)).ToString () + " s";
			finishSpeedText.text = LanguageManager.GetString (25) + ": " +((int)(rotator.recordSpeed/60f)).ToString() + " / min";
			finishTurnsText.text = LanguageManager.GetString (26) + ": " + rotator.turns.ToString();
			finishGoldText.text = LanguageManager.GetString (27) + ": "+rotator.totalGold.ToString();
			finishTimeText.color = (rotator.timeRot > CoreManager.profile.maxTime ? recordColor : stdColor);
			finishGoldText.color = (rotator.totalGold > CoreManager.profile.maxGoldEarned ? recordColor : stdColor);
			finishSpeedText.color = (rotator.recordSpeed/60f > CoreManager.profile.maxSpeed ? recordColor : stdColor);
			finishTurnsText.color = (rotator.turns > CoreManager.profile.maxTurns ? recordColor : stdColor);
			if (rotator.timeRot > CoreManager.profile.maxTime) {
				CoreManager.profile.maxTime = rotator.timeRot;
			}
			if (rotator.recordSpeed > CoreManager.profile.maxSpeed) {
				CoreManager.profile.maxSpeed = rotator.recordSpeed/60f;
			}
			if (rotator.turns > CoreManager.profile.maxTurns) {
				CoreManager.profile.maxTurns = rotator.turns;
			}
			if (rotator.totalGold > CoreManager.profile.maxGoldEarned) {
				CoreManager.profile.maxGoldEarned = rotator.totalGold;
			}
			if (record) {
				CoreManager.WriteSave ();
				OnProfileUpdate ();
			}
		}

		void OnLongLack() {
			FinishPlay ();
		}

		void OnAppBonus(AppBonus bonus) {
			string str = LanguageManager.GetString (34);
			str=str.Replace ("{0}", bonus.name);
			str=str.Replace ("{1}", NLExtensions.StringExtensions.LongToStr(bonus.bonus));
			AddNotify (str);
		}

		void Update() {
			if (rotator.play) {
				speedText.text = ((int)(rotator.GetSpeedInMin ())).ToString () + " / min";
				turnsText.text = LanguageManager.GetString (20) + " " + NLExtensions.StringExtensions.IntToStr(rotator.turns);
			}
		}

		void RefreshReward(bool value=false) {
			watchAdButton.enable = AdManager.rewardReady;
		}

		void AddNotify(string str) {
			notifyQueue.Add (str);
			if (state != State.play && state != State.finishPlay && state != State.pause && state != State.notify) {
				NextNotify ();
			}
		}

		void NextNotify() {
			if (notifyQueue.Count > 0) {
				notifyText.text = notifyQueue [0];
				state = State.notify;
				notifyQueue.RemoveAt (0);
			} else
				state = oldState;
		}

		void OnAvailableApps() {
			otherAppsButton.SetActive (AchievmentManager.appsToInstall.Count>0);
			if (AchievmentManager.appsToInstall.Count > 0) {
				appController.Refresh (AchievmentManager.appsToInstall);
			}
		}

		void OnPurchase(bool success, long gold) {
			string str = string.Empty;
			if (success && gold>0) {
				str = LanguageManager.GetString (17);
				str = str.Replace ("1", gold.ToString ());
			} else {
				str = LanguageManager.GetString (18);
			}
			AddNotify (str);
			//notifyText.text = str;
			//state = State.notify;
		}

		void OnProfileUpdate() {
			goldText.text = NLExtensions.StringExtensions.LongToStrUniversal (CoreManager.gold, 99999);
			recordGoldText.text = LanguageManager.GetString (30) + ": " + NLExtensions.StringExtensions.LongToStr (CoreManager.profile.maxGoldEarned);
			recordTurnsText.text = LanguageManager.GetString (31) + ": " + NLExtensions.StringExtensions.LongToStr (CoreManager.profile.maxTurns);
			recordSpeedText.text = LanguageManager.GetString (32) + ": " + NLExtensions.StringExtensions.IntToStr ((int)(CoreManager.profile.maxSpeed));
			recordTimeText.text = LanguageManager.GetString (33) + ": " + TimeFromStr(CoreManager.profile.maxTime);
		}

		void OnLanguageChanged() {
			OnProfileUpdate ();
		}

		string TimeFromStr(float s) {
			TimeSpan t = TimeSpan.FromSeconds (s);
			string str = string.Empty;
			if (t.Hours > 0)
				str += t.Hours.ToString () + " H";
			if (t.Minutes>0)
				str += " "+t.Minutes.ToString () + " m";
			if (t.Seconds>0 || (t.Hours==0 && t.Minutes==0))
				str += " "+t.Seconds.ToString () + " s";
			return str;
		}

		void Start() {
			SwitchOffAllCanvasGroups ();
			state = State.main;
			oldState = State.main;
			OnProfileUpdate ();
			RefreshReward ();
			OnAvailableApps ();
			versionText.text = "v"+ServiceConfig.version.ToString ();
		}

		void OnEnable() {
			instance = this;
			CoreManager.onProfileUpdate += OnProfileUpdate;
			InputManager.onReturn += OnReturn;
			AdManager.onSuccessReward += OnSuccessReward;
			AdManager.onRewardStatusChanged += RefreshReward;
			PurchaseManager.onPurchase += OnPurchase;
			LanguageManager.onLanguageChanged += OnLanguageChanged;
			AchievmentManager.onBonus += OnAppBonus;
			AchievmentManager.onAvailableApps += OnAvailableApps;
			rotator.onLongLack += OnLongLack;
			rotator.onGoldAdd += OnAddPlayGold;
		}

		void OnDisable() {
			CoreManager.onProfileUpdate -= OnProfileUpdate;
			InputManager.onReturn -= OnReturn;
			AdManager.onSuccessReward -= OnSuccessReward;
			AdManager.onRewardStatusChanged -= RefreshReward;
			PurchaseManager.onPurchase -= OnPurchase;
			LanguageManager.onLanguageChanged -= OnLanguageChanged;
			AchievmentManager.onBonus -= OnAppBonus;
			AchievmentManager.onAvailableApps -= OnAvailableApps;
			rotator.onLongLack -= OnLongLack;
			rotator.onGoldAdd -= OnAddPlayGold;
		}

		void OnValidate() {
			SwitchOffAllCanvasGroups ();
			state = state;
		}

		void OnSuccessReward(bool click) {
			CoreManager.AddRewardAd (click);
		}

		CanvasGroup GetScreen(State st) {
			switch (st) {
			case State.main:
				return mainScreen;
			case State.play:
				return playScreen;
			case State.spinners:
				return spinnersScreen;
			case State.shop:
				return shopScreen;
			case State.notify:
				return notifyScreen;
			case State.pause:
				return pauseScreen;
			case State.finishPlay:
				return finishPlayScreen;
			case State.otherApps:
				return otherAppsScreen;
			case State.settings:
				return settingsScreen;
			default:
				return null;
			}
		}

		void SwitchOffAllCanvasGroups()
		{
			var values = Enum.GetValues(typeof(State));
			for (int i = 0; i < values.Length; i++) {
				GetScreen ((State)(values.GetValue(i))).Switch (false);
			}
		}
		#endregion

		#region Events
		public static Action<State> onSwitchScreen;
		#endregion
	}
}