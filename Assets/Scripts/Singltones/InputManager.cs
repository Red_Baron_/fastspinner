﻿/*Copyright (C) GameStudio316
*/
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FastSpinner {
	public class InputManager : MonoBehaviour {
		#region Data
		[SerializeField] int samplesCount=100;
	    [SerializeField] float touchRatio = 1f;
		static bool _isRotating=false;
		static float _magnitude=0f;
		static RotateDirection _rotateDir=RotateDirection.Left;
		static Vector3 oldMousePos;
	
		private bool swiping = false;
		private bool eventSent = false;
		private Vector2 lastPosition;

		Vector2 lastPos;
		#endregion

		#region Methods
		void Start () {
			#if UNITY_EDITOR || UNITY_STANDALONE
			oldMousePos = Input.mousePosition;
			#endif
		}

		void Update () {
			if (TechManager.isEditor || TechManager.platform == Platform.Windows) {
				bool click = Input.GetMouseButton (0);
				magnitude = (click ? (Input.mousePosition - oldMousePos).magnitude : 0f);
				isRotating = (click && magnitude > 0f);
				oldMousePos = Input.mousePosition;
			} else {
				isRotating = (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved);
				magnitude = (Input.touchCount>0 ? Input.GetTouch (0).deltaPosition.magnitude*touchRatio : 0f);
			}
			if (Input.GetKeyUp(KeyCode.Escape) && onReturn != null)
				onReturn();
			CheckSwipe ();
			if (ViewManager.instance.state == State.play && isRotating) {
				CalcDir ();
			}
			lastPos= GetPos ();
		}
			
		List<float> angles = new List<float>();

		void CalcDir() {
			float angle = GetAngle (GetPos (), lastPos);
			angles.Add (angle);
			if (angles.Count > samplesCount) {
				angles.RemoveAt (0);
			}
			int bigger = 0;
			for (int i = 1; i < angles.Count; i++) {
				if ((angles[i]-angles[i-1])>=0f)
					bigger++;
			}
			rotateDir = (bigger>samplesCount/2 ? RotateDirection.Right : RotateDirection.Left);
		}

		float GetAngle(Vector2 vec1, Vector2 vec2) {
			float angle = Mathf.Atan2 (vec2.y - vec1.y, vec2.x - vec1.x) * 180f / Mathf.PI;
			return angle;
		}

		Vector2 GetPos() {
			if (TechManager.isEditor || TechManager.platform == Platform.Windows) {
				return Input.mousePosition;
			} else if (Input.touches.Length > 0)
				return Input.GetTouch (0).position;
			return lastPos;
		}

		void CheckSwipe () {
			if (Input.touchCount == 0) 
				return;
			if (Input.GetTouch(0).deltaPosition.sqrMagnitude != 0){
				if (swiping == false){
					swiping = true;
					lastPosition = Input.GetTouch(0).position;
					return;
				}
				else{
					if (!eventSent) {
						if (onSwipe != null) {
							Vector2 direction = Input.GetTouch(0).position - lastPosition;

							if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)){
								if (direction.x > 0) 
									onSwipe(SwipeDirection.Left);
								else
									onSwipe(SwipeDirection.Right);
							}
							else{
								if (direction.y > 0)
									onSwipe(SwipeDirection.Up);
								else
									onSwipe(SwipeDirection.Down);
							}

							eventSent = true;
						}
					}
				}
			}
			else{
				swiping = false;
				eventSent = false;
			}
		}
		#endregion

		#region Interface
		public static RotateDirection rotateDir {
			get {
				return _rotateDir;
			}
			private set {
				_rotateDir = value;
			}
		}

		public static bool isRotating {
			get {
				return _isRotating;
			}
			private set {
				_isRotating = value;
			}
		}

		public static float magnitude {
			get {
				return _magnitude;
			}
			private set {
				_magnitude = value;
			}
		}
		#endregion

		#region Events
		public static Action onReturn;
		public static Action<SwipeDirection> onSwipe;
		#endregion
	}
}