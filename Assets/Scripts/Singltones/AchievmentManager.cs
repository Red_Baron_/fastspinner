﻿/*Copyright (C) GameStudio316
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using NLExtensions;

namespace FastSpinner {
	public class AchievmentManager : MonoBehaviour {
		public static List<AppBonus> appsToInstall = new List<AppBonus> ();
		static bool refreshing = false;

		public static bool hasAvailableApps {
			get {
				return appsToInstall.Count > 0;
			}
		}

		void Start() {
			StartCoroutine(Delay(Refresh, 1f));
		}

		void OnEnable() {
			TechManager.onFocus += OnFocus;
		}

		void OnDisable() {
			TechManager.onFocus -= OnFocus;
		}

		IEnumerator Delay(Action action, float delay) {
			yield return new WaitForSeconds (delay);
			if (action != null)
				action ();
		}

		void OnFocus(bool focus) {
			if (focus)
				Refresh ();
		}

		public static void Refresh() {
			if (!refreshing) {
				refreshing = true;
				bool installed = false;
				bool contains = false;
				appsToInstall.Clear ();
				for (int i = 0; i < OtherAppConfig.apps.Count; i++) {
					installed = TechExtensions.IsAppInstalled (OtherAppConfig.apps [i].bundleId);
					contains = CoreManager.profile.appBonuses.Contains (OtherAppConfig.apps [i].bundleId);
					if (!contains && installed) {
						Logger.Log ("AchievmentManager: new app bonus " + OtherAppConfig.apps [i]);
						if (onBonus != null)
							onBonus (OtherAppConfig.apps [i]);
						CoreManager.AddAppBonus ((OtherAppConfig.apps [i]));
					}
					if (!contains && !installed) {
						appsToInstall.Add (OtherAppConfig.apps [i]);
					}
				}
                if (appsToInstall.Count>0)
				    Logger.Log ("AchievmentManager: scanned apps to install: " + ListToStr (appsToInstall));
				if (appsToInstall.Count > 0 && onAvailableApps != null) {
					onAvailableApps ();
				}
				refreshing = false;
			}
		}

		static string ListToStr(List<AppBonus> list) {
			string str = "[";
			for (int i = 0; i < list.Count; i++) {
				str += list [i].bundleId;
				if (i < list.Count - 1)
					str += ",";
			}
			str += "]";
			return str;
		}

		public static Action onAvailableApps;
		public static Action<AppBonus> onBonus;
	}
}