﻿/*Copyright (C) GameStudio316
  License: http://aziworldclub.com/img/sogl.pdf
*/
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
 //You must obfuscate your secrets using Window > Unity IAP > Receipt Validation Obfuscator
 //before receipt validation will compile in this sample.
 #define RECEIPT_VALIDATION
#endif

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
#if RECEIPT_VALIDATION
using UnityEngine.Purchasing.Security;
#endif

namespace FastSpinner.Payments {
	// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
	public class Purchaser : MonoBehaviour, IStoreListener
	{

		#if RECEIPT_VALIDATION
		private CrossPlatformValidator validator;
		#endif

		#pragma warning disable 0414
		private bool m_IsGooglePlayStoreSelected;
		#pragma warning restore 0414

		private static IStoreController m_StoreController;          // The Unity Purchasing system.
		private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

		// Product identifiers for all products capable of being purchased: 
		// "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
		// counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
		// also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

		// General product identifiers for the consumable, non-consumable, and subscription products.
		// Use these handles in the code to reference which product to purchase. Also use these values 
		// when defining the Product Identifiers on the store. Except, for illustration purposes, the 
		// kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
		// specific mapping to Unity Purchasing's AddProduct, below.

		public static Action<bool, string, string, string> onPurchase;

		int initFailCount=0;

		static Purchaser _instance;
		public static Purchaser instance {
			get {
				return _instance;
			}
		}

		void OnEnable() {
			_instance = this;
		}

		void Start()
		{
			Init ();
		}

		void Init() {
			#if !NO_SHOP && (!UNITY_STANDALONE || UNITY_EDITOR)
			// If we haven't set up the Unity Purchasing reference
			if (m_StoreController == null)
			{
				// Begin to configure our connection to Purchasing
				InitializePurchasing();
			}
			#else
			Logger.Log("Purchaser: not initialized on standalone.");
			#endif
		}

		public void InitializePurchasing() 
		{
			// If we have already connected to Purchasing ...
			if (IsInitialized())
			{
				// ... we are done here.
				return;
			}
			var module = StandardPurchasingModule.Instance();
			// Create a builder, first passing in a suite of Unity provided stores.
			var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

			// Add a product to sell / restore by way of its identifier, associating the general identifier
			// with its store-specific identifiers.
			//builder.AddProduct(kProductIDConsumable, ProductType.Consumable);

			// Continue adding the non-consumable product.
			//builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable);

			// And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
			// if the Product ID was configured differently between Apple and Google stores. Also note that
			// one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
			// must only be referenced here. 
			if (PaymentConfig.goods != null && PaymentConfig.goods.Count > 0) {
				try {
					for (int i = 0; i < PaymentConfig.goods.Count; i++) {
						//Logger.Log ("Purchaser: adding product " + PaymentConfig.goods [i]);
						builder.AddProduct (PaymentConfig.goods [i].id, ProductType.Consumable, PaymentConfig.goods [i].GetIDs ());
					}
				}
				catch {
					Logger.LogError ("Purchaser: cannot add goods.");
				}
			}
			#if RECEIPT_VALIDATION
			validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), identifier);
			#endif
			builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2O/9/H7jYjOsLFT/uSy3ZEk5KaNg1xx60RN7yWJaoQZ7qMeLy4hsVB3IpgMXgiYFiKELkBaUEkObiPDlCxcHnWVlhnzJBvTfeCPrYNVOOSJFZrXdotp5L0iS2NVHjnllM+HA1M0W2eSNjdYzdLmZl1bxTpXa4th+dVli9lZu7B7C2ly79i/hGTmvaClzPBNyX+Rtj7Bmo336zh2lYbRdpD5glozUq+10u91PMDPH+jqhx10eyZpiapr8dFqXl5diMiobknw9CgcjxqMTVBQHK6hS0qYKPmUDONquJn280fBs1PTeA6NMG03gb9FLESKFclcuEZtvM8ZwMMRxSLA9GwIDAQAB");
			m_IsGooglePlayStoreSelected = Application.platform == RuntimePlatform.Android && module.androidStore == AndroidStore.GooglePlay;
			// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
			// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
			UnityPurchasing.Initialize(this, builder);
			//Logger.Log ("Purchaser: initialized.");
		}

		static string identifier {
			get {
				#if UNITY_2017_1_OR_NEWER
				return Application.identifier;
				#else 
				return Application.bundleIdentifier;
				#endif
			}
		}

		private bool IsInitialized()
		{
			// Only say we are initialized if both the Purchasing references are set.
			return m_StoreController != null && m_StoreExtensionProvider != null;
		}


		public void BuyProduct(string id)
		{
			// Buy the non-consumable product using its general identifier. Expect a response either 
			// through ProcessPurchase or OnPurchaseFailed asynchronously.
			if (IsInitialized ()) {
				for (int i = 0; i < PaymentConfig.goods.Count; i++) {
					if (PaymentConfig.goods [i].id.Equals(id)) {
						BuyProductID (PaymentConfig.goods [i].id);
						return;
					}
				}
			} else {
				Logger.Log ("Purchaser: cannot buy product because not initialized.");
				if (onPurchase != null)
					onPurchase (false, id, string.Empty, string.Empty);
				Init ();
			}
		}


		void BuyProductID(string productId)
		{
			// If Purchasing has been initialized ...
			if (IsInitialized())
			{
				// ... look up the Product reference with the general product identifier and the Purchasing 
				// system's products collection.
				Product product = m_StoreController.products.WithID(productId);

				// If the look up found a product for this device's store and that product is ready to be sold ... 
				if (product != null && product.availableToPurchase)
				{
					Logger.Log(string.Format("Purchaser: purchasing product ["+productId+"] asychronously: '{0}'", product.definition.id));
					// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
					// asynchronously.
					m_StoreController.InitiatePurchase(product);
				}
				// Otherwise ...
				else
				{
					// ... report the product look-up failure situation  
					Logger.LogError("Purchaser: BuyProductID ["+productId+"] FAIL. Not purchasing product, either is not found or is not available for purchase.");
				}
			}
			// Otherwise ...
			else
			{
				// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
				// retrying initiailization.
				Logger.LogError ("Purchaser: cannot buy "+productId+" because not initialized.");
			}
		}


		// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
		// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
		public void RestorePurchases()
		{
			// If Purchasing has not yet been set up ...
			if (!IsInitialized())
			{
				// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
				Logger.LogError("Purchaser: restorePurchases FAIL. Not initialized.");
				return;
			}

			// If we are running on an Apple device ... 
			if (Application.platform == RuntimePlatform.IPhonePlayer || 
				Application.platform == RuntimePlatform.OSXPlayer)
			{
				// ... begin restoring purchases
				Logger.Log("Purchaser: RestorePurchases started ...");

				// Fetch the Apple store-specific subsystem.
				var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
				// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
				// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
				apple.RestoreTransactions((result) => {
					// The first phase of restoration. If no more responses are received on ProcessPurchase then 
					// no purchases are available to be restored.
					Logger.Log("Purchaser: RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				});
			}
			// Otherwise ...
			else
			{
				// We are not running on an Apple device. No work is necessary to restore purchases.
				Logger.Log("Purchaser: RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
			}
		}


		//  
		// --- IStoreListener
		//

		public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
		{
			// Purchasing has succeeded initializing. Collect our Purchasing references.

			// Overall Purchasing system, configured with products for this application.
			m_StoreController = controller;
			// Store specific subsystem, for accessing device-specific store features.
			m_StoreExtensionProvider = extensions;
			Logger.Log("Purchaser: initialized.");
		}


		public void OnInitializeFailed(InitializationFailureReason error)
		{
			// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
			Logger.LogError("Purchaser: OnInitializeFailed InitializationFailureReason:" + error);
			initFailCount++;
			if (initFailCount < 30) {
				Logger.Log ("Purchaser: retry purchasing init. Tries: "+initFailCount+".");
				StartCoroutine (Delay (InitializePurchasing, 1f));
			}
			else Logger.Log ("Purchaser: init tries: "+initFailCount.ToString()+". Stopping.");
		}


		IEnumerator Delay(Action action, float delay) {
			yield return new WaitForSeconds (delay);
			if (action != null)
				action ();
		}

		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
		{

			var finded = PaymentConfig.goods.Find (x => String.Equals (args.purchasedProduct.definition.id, x.id, StringComparison.Ordinal));
			string id = string.Empty;
			string transactionID = args.purchasedProduct.transactionID;
			string token = GetToken (args);
			if (finded != null) {
				id = finded.id;
				Logger.Log ("Purchaser: good [" + finded.id + "] successfully purchased. Transaction id: "+transactionID+", token: "+token+".");
			}
			// Or ... an unknown product has been purchased by this user. Fill in additional products here....
			else {
				Logger.Log (string.Format ("Purchaser: ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));

			}
			if (onPurchase != null)
				onPurchase (true, id, transactionID, token);
			// Return a flag indicating whether this product has completely been received, or if the application needs 
			// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
			// saving purchased products to the cloud, and when that save is delayed. 
			return PurchaseProcessingResult.Complete;
		}



		string GetToken(PurchaseEventArgs e) {
			string token = string.Empty;
			#if RECEIPT_VALIDATION
			// Local validation is available for GooglePlay and Apple stores
			if (m_IsGooglePlayStoreSelected ||
				Application.platform == RuntimePlatform.IPhonePlayer ||
				Application.platform == RuntimePlatform.OSXPlayer ||
				Application.platform == RuntimePlatform.tvOS) {
				try {
					var result = validator.Validate(e.purchasedProduct.receipt);
					Logger.Log("Purchaser: receipt is valid.");
					foreach (IPurchaseReceipt productReceipt in result) {
						#if UNITY_ANDROID
						GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
						if (null != google) {
							if (!string.IsNullOrEmpty(google.purchaseToken))
								token = google.purchaseToken;
						}
						#endif
						#if UNITY_IOS
						AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
						if (apple!=null) {
							token = apple.productID;
							//Logger.Log(apple.originalTransactionIdentifier);
							//Logger.Log(apple.subscriptionExpirationDate);
							//Logger.Log(apple.cancellationDate);
							//Logger.Log(apple.quantity);
						}
						#endif
					}
				} catch (IAPSecurityException) {
					Logger.Log("Purchaser: invalid receipt.");
					//return PurchaseProcessingResult.Complete;
				}
			}
			#endif
			return token;
		}


		public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
		{
			// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
			// this reason with the user to guide their troubleshooting actions.
			var finded = PaymentConfig.goods.Find (x => String.Equals (product.definition.id, x.id, StringComparison.Ordinal));
			string id = string.Empty;
			if (finded != null) {
				id = finded.id;
			}
			Logger.Log(string.Format("Purchaser: OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
			if (onPurchase != null)
				onPurchase (false, id, null, null);
		}
	}
}