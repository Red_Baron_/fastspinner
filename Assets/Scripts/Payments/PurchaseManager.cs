﻿/*Copyright (C) GameStudio316
*/
using System;

namespace FastSpinner.Payments {
	public static class PurchaseManager{
		#region Data
	    static bool init = false;
		#endregion

		#region Methods
	    static void Init()
	    {
	        if (!init)
	        {
	            Purchaser.onPurchase += OnPurchase;
	            init = true;
	        }
	    }

		static void OnPurchase (bool success, string id, string transactionId="", string purchaseToken="") {
			var f = PaymentConfig.goods.Find (x => x.id == id);
			long gold = 0;
			if (f != null)
				gold = f.gold;
			if (gold == 0)
				success = false;
			if (success) {
				Logger.Log ("PurchaseManager: payment [id: " + id + "] to store successfull. Gold: "+gold);
			} else {
				Logger.Log ("PurchaseManager: error for payment [id: " + id + "].");
			}
			if (onPurchase != null) {
				onPurchase (success, gold);
			}
			if (success && gold > 0) {
				CoreManager.BuyGold (gold);
			}
		}
		#endregion

		#region Interface
		public static void Buy(string id) {
            Init();
			var finded = PaymentConfig.goods.Find (x => x.id == id);
			if (finded != null) {
				Logger.Log ("PurchaseManager: purchasing item [id: " + id + "].");
				if (TechManager.isEditor || TechManager.platform == Platform.Windows) {
					Logger.Log ("PurchaseManager: imitating purchase in editor or standalone.");
					bool success = (UnityEngine.Random.Range (0, 2) >=1 ? true : false);
					OnPurchase (success, id);
				} else {
					Purchaser.instance.BuyProduct (id);
				}
			} else {
				Logger.LogError ("PurchaseManager: cannot buy item [id: " + id + "]: PaymentConfig does not contains item.");
			}
		}
		#endregion

		#region Events
		public static Action<bool, long> onPurchase;
		#endregion
	}
}