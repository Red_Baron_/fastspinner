﻿/*Copyright (C) GameStudio316
*/
using System.Collections.Generic;
using UnityEngine.Purchasing;

namespace FastSpinner.Payments {
	[System.Serializable]
	public class StoreGood {
		public string id=string.Empty;
		public string googlePlayID=string.Empty;
		public string appleID=string.Empty;
		public long gold = 0;

		public StoreGood(string id) {
			this.id = id;
			googlePlayID = id;
			appleID = id;
		}

		public StoreGood(string id, string googlePlayID) {
			this.id = id;
			this.googlePlayID = googlePlayID;
			appleID = string.Empty;
		}

		public StoreGood(string id, string googlePlayID, string appleID) {
			this.id = id;
			this.googlePlayID = googlePlayID;
			this.appleID = appleID;
		}

		public IDs GetIDs () {
			return new IDs () {
				{googlePlayID, GooglePlay.Name},
				{appleID, AppleAppStore.Name}
			};
		}

		public override string ToString ()
		{
			return string.Format ("id={0}, googleId={1}, appleId={2}, IDs={3}", id, googlePlayID, appleID, GetIDs());
		}
	}
}

