﻿/*Copyright (C) GameStudio316
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using NLExtensions;

namespace FastSpinner.Scroll {
	public class ScrollPage : MonoBehaviour, ILayoutElement, IScrollablePage {
		#region Unity scene settings
		[Header("Optional")]
		[SerializeField] RectTransform sizeSource;
		#endregion

		#region Interface
		public event Action OnScrollToThis;
		#endregion

		#region Data
		public RectTransform SizeSource {
			get {
				if (sizeSource == null) sizeSource = GetComponentInParent<ScrollRect>().transform as RectTransform;
				return sizeSource;
			}
			set { sizeSource = value; }
		}
		#endregion

		#region Члены IScrollablePage
		RectTransform IScrollablePage.SizeSource {
			get {
				return sizeSource;
			}
			set {
				sizeSource = value;
			}
		}

		public void OnPageSetted () {
			if ( OnScrollToThis != null ) OnScrollToThis ();
		}
		#endregion

		#region ILayoutElement Members
		public void CalculateLayoutInputHorizontal () {
		}

		public void CalculateLayoutInputVertical () {
		}

		public float flexibleHeight {
			get { return 0; }
		}

		public float flexibleWidth {
			get { return 0; }
		}

		public int layoutPriority {
			get { return 1; }
		}

		public float minHeight {
			get {
				if (sizeSource != null) return SizeSource.GetHeight ();
				else return 0;
			}
		}

		public float minWidth {
			get {
				if (sizeSource != null) return SizeSource.GetWidth ();
				else return 0;
			}
		}

		public float preferredHeight {
			get {
				if (sizeSource != null) return SizeSource.GetHeight ();
				else return 0;
			}
		}

		public float preferredWidth {
			get {
				if (sizeSource != null) return SizeSource.GetWidth ();
				else return 0;
			}
		}
		#endregion
	}
}