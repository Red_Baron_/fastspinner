﻿/*Copyright (C) GameStudio316
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System;

namespace FastSpinner.Scroll {
	public class PageScroller : MonoBehaviour {
		#region Unity scene settings
		[SerializeField] float scrollSpeed = 5f;
		[SerializeField] bool startFromLeftBorder = true;
		[SerializeField] GameObject CollectionOfImages;      //если надо искать объекты для скролла не в своих чайлдах, а в чайлдах указанного объекта 
		[SerializeField] ScrollRect scrollRect;
		[SerializeField] PageScrollerType scrollType = PageScrollerType.Horizontal;
		#endregion

		#region Data
		bool valid;
		float pageOffset;
		const float LeftBorder = 0;
		const float RightBorder = 1;
		IScrollablePage[] pages;
		int currentPage = 0;
		Func<float> GetScrollPosition;
		Action<float> SetScrollPosition;
		#endregion

		#region Interface
		public event Action<int> BeforePageSetted;
		public event Action<int> AfterPageSetted;
		public bool isMoving;
		public int PageCount {
			get {
				return pages.Length;
			}
		}
		public int CurrentPage {
			get { return currentPage; }
			set { SetPage (value, true); }
		}

		public PageScrollerType ScrollType {
			get { return scrollType; }
			set { scrollType = value; }
		}

		public float ScrollPosition {
			get { return GetScrollPosition(); } 
			private set { SetScrollPosition(value); }
		}

		public void SetCollection ( GameObject collection ) {
			CollectionOfImages = collection;
		}

		public void Refresh () {
			pages = null;
			Awake ();             //т.к. коллекции картинок включаются и выключаются, необходимо обновление
			Start ();
		}

		public void MoveLeft ( Action onBorderClash, bool immediate = false, int movePages = 1 ) {
			if ( valid ) {
				StopAllCoroutines ();
				if ( ScrollPosition < 0.0001 ) {
					if ( onBorderClash != null ) onBorderClash ();
				}
				float newPos = Mathf.Clamp01(ScrollPosition - pageOffset * movePages);
				//Debug.Log (newPos);
				StartCoroutine (MoveTo (newPos, immediate));
			}
		}


		public void MoveRight ( Action onBorderClash, bool immediate = false, int movePages = 1) {
			if ( valid ) {
				StopAllCoroutines ();
				if ( ScrollPosition > 0.9999 ) {
					if ( onBorderClash != null ) onBorderClash ();
				}
				float newPos = Mathf.Clamp01(ScrollPosition + pageOffset * movePages);
				StartCoroutine (MoveTo (newPos, immediate));
			}
		}

		public void SetPage ( int page, bool immediate = false ) {
			if ( valid ) {
				StopAllCoroutines ();
				page = Mathf.Clamp (page, 0, PageCount - 1);
				currentPage = page;
				float newPos = Mathf.Clamp01 (pageOffset * page);
				StartCoroutine (MoveTo (newPos, immediate));
			}
		}


		#if UNITY_EDITOR
		public void MoveLeft () {
			MoveLeft (null);
		}
		public void MoveRight () {
			MoveRight (null);
		}
		#endif
		#endregion

		#region Methods
		public void Awake () {
			if ( ScrollType == PageScrollerType.Horizontal ) {
				GetScrollPosition = () => scrollRect.horizontalNormalizedPosition;
				SetScrollPosition = (float e) => scrollRect.horizontalNormalizedPosition = e;
			} else if ( ScrollType == PageScrollerType.Vertical ) {
				GetScrollPosition = () => 1f - scrollRect.verticalNormalizedPosition;
				SetScrollPosition = ( float e ) => scrollRect.verticalNormalizedPosition = 1f - e;
			}
			if ( scrollRect == null ) scrollRect = GetComponentInParent<ScrollRect> ();
			if ( pages == null || pages.Length == 0 ) {
				if ( CollectionOfImages != null ) {
					pages = CollectionOfImages.GetComponentsInChildren<IScrollablePage> ();
					if ( scrollRect ) scrollRect.content = CollectionOfImages.GetComponent<RectTransform> ();
				} else pages = GetComponentsInChildren<IScrollablePage> ();
			}
			if ( pages != null && pages.Length > 1 && pages.All (e => e != null) ) {
				if ( scrollRect != null ) {
					valid = true;
					ScrollPosition = 0;
					pageOffset = 1f / ( pages.Length - 1 );
				} else {
					Debug.LogWarning ("Scroll rect not found. Component wont work");
					valid = false;
				}
			} else {
				//Debug.LogWarning ("Insufficient pages. Need 2 or more");
				valid = false;
			}
			if ( scrollRect != null ) ScrollPosition = LeftBorder;
		}

		public void Start () {
			if ( startFromLeftBorder ) {
				ScrollPosition = LeftBorder;
			}
		}

		private IEnumerator MoveTo ( float newPos, bool immediate ) {
			currentPage = Mathf.RoundToInt (newPos * (PageCount-1));
			if ( BeforePageSetted != null ) { BeforePageSetted (currentPage); }
			isMoving = true;
			float diff = newPos - ScrollPosition;
			float direction = Mathf.Sign (diff);
			if ( scrollSpeed != 0 && diff != 0 ) {
				if ( !immediate ) {
					while ( Mathf.Sign (diff) == direction ) {
						yield return null;
						ScrollPosition += scrollSpeed * direction * Time.deltaTime / pages.Length;
						diff = newPos - ScrollPosition;
					}
				}
				ScrollPosition = newPos;
			}
			if ( AfterPageSetted != null ) { AfterPageSetted (currentPage); }
			pages[currentPage].OnPageSetted ();
			isMoving = false;
		}
		#endregion
	}
}