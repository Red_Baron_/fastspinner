﻿/*Copyright (C) GameStudio316
*/
namespace FastSpinner.Scroll {
	public enum PagePosition { Left, Center, Right }
	public enum PageScrollerType { Vertical, Horizontal }
}