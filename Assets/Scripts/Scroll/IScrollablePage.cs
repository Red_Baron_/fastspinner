﻿/*Copyright (C) GameStudio316
*/
using UnityEngine.UI;
using UnityEngine;
using System;

namespace FastSpinner.Scroll {
	public interface IScrollablePage : ILayoutElement {
		RectTransform SizeSource {
			get;
			set;
		}
		void OnPageSetted();
	}
}