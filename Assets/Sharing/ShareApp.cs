﻿namespace Sharing
{
    public enum ShareApp {
        Whatsapp = 1,
        Viber = 2,
        Telegram = 3,
        Twitter =4
    }
}