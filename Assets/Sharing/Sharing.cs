﻿// vk, fb, ok, tw, wa, vb, tg
//twitter, whatsapp, viber, telegram - not-reference working
using UnityEngine;
using FastSpinner;
#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

namespace Sharing
{
    public class SharingManager : MonoBehaviour
    {

#if UNITY_IOS
		[DllImport ("__Internal")]
		private static extern void shareVia (string app, string message, string url, string param);
#endif

        // Поделиться 
        public static void ShareVia(ShareApp appType, string message, string url, string param = "")
        {
#if UNITY_ANDROID || UNITY_IOS
            string app = GetAppId(appType);
#endif
#if UNITY_ANDROID
			message = string.Format ("{0} {1}", message, url); // добавление ссылки
			using (var plugin = new AndroidJavaClass("com.mycompany.sharing.Plugin")) {
				plugin.CallStatic("shareVia", app, message);
			}
#elif UNITY_IOS
			shareVia (app, message+" "+url, url, param);
#endif
        }

        static string GetAppId(ShareApp app)
        {
            switch (app)
            {
               case ShareApp.Twitter:
                   return "tw";
               case ShareApp.Whatsapp:
                   return "wa";
                case ShareApp.Viber:
                    return "vb";
                case ShareApp.Telegram:
                    return "tg";
                default:
                    return string.Empty;
            }
        }

        // Не удалось расшарить
        void OnShareError(string result)
        {
            switch (result)
            {
                case "NotInstall":
                    // приложение не установлено
                    FastSpinner.Logger.Log("приложение не установлено");
                    break;
                case "NotAvailable":
                    // шаринг не доступен
                    FastSpinner.Logger.Log("шаринг не доступен");
                    break;
                case "AccessDenied":
                    // нет доступа
                    FastSpinner.Logger.Log("нет доступа");
                    break;
                default:
                    // не удалось расшарить текст
                    FastSpinner.Logger.Log("не удалось расшарить");
                    break;
            }
        }
    }
}