﻿namespace Sharing
{
    public class ShareConfig
    {
        // Название приложения
        public const string appName = "Azi World Club";
        // ID приложения для шаринга в ФБ
        public const string FacebookAppId = "000";
        // ID приложения для шаринга в ВК
        public const string VKontakteAppId = "000";
        // ID приложения для шаринга в Одноклассниках
        public const string OdnoklassnikiAppId = "000";
        // Secret ID приложения для шаринга в Одноклассниках
        public const string OdnoklassnikiSecretId = "000";
    }
}