﻿#if UNITY_EDITOR && UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using UnityEditor.iOS.Xcode;
using System.Collections.Generic;

namespace Sharing
{
    public class iOSPostBuildProcess
    {
        public const string frameworksPath = "iOSFrameworks";

        public static string fullFrameworksPath
        {
            get { return Path.Combine(Application.dataPath, frameworksPath); }
        }

        [PostProcessBuild]
        public static void iOSPostProcess(BuildTarget buildTarget, string pathToBuiltProject)
        {
            if (buildTarget == BuildTarget.iOS)
            {
                FixPlist(pathToBuiltProject);
                //AddFrameworks(pathToBuiltProject);
            }
        }

        // Добавляем параметры в Info.plist
        private static void FixPlist(string path)
        {
            string plistPath = path + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));
            PlistElementDict rootDict = plist.root;
            rootDict.SetString("FacebookAppID", ShareConfig.FacebookAppId);
			rootDict.SetString ("NSCameraUsageDescription", "not used");
			rootDict.SetString ("NSCalendarsUsageDescription", "not used");
            rootDict.SetString("FacebookDisplayName", ShareConfig.appName);
            PlistElementArray queriesSchemes = rootDict.CreateArray("LSApplicationQueriesSchemes");
            queriesSchemes.AddString("fb");
            queriesSchemes.AddString("fbapi");
            queriesSchemes.AddString("fbapi20130214");
            queriesSchemes.AddString("fbapi20130410");
            queriesSchemes.AddString("fbapi20130702");
            queriesSchemes.AddString("fbapi20131010");
            queriesSchemes.AddString("fbapi20131219");
            queriesSchemes.AddString("fbapi20140410");
            queriesSchemes.AddString("fbapi20140116");
            queriesSchemes.AddString("fbapi20150313");
            queriesSchemes.AddString("fbapi20150629");
            queriesSchemes.AddString("fbauth");
            queriesSchemes.AddString("fbauth2");
            queriesSchemes.AddString("fbshareextension");
            queriesSchemes.AddString("fb-messenger-api20140430");
            queriesSchemes.AddString("vk");
            queriesSchemes.AddString("vk-share");
            queriesSchemes.AddString("vkauthorize");
            queriesSchemes.AddString("viber");
            queriesSchemes.AddString("whatsapp");
            queriesSchemes.AddString("tg");
            queriesSchemes.AddString("okauth");
            queriesSchemes.AddString(string.Format("ok{0}", ShareConfig.OdnoklassnikiAppId));
            PlistElementArray typesArray = rootDict.CreateArray("CFBundleURLTypes");
            PlistElementDict urlsDict = typesArray.AddDict();
            PlistElementArray urlsArray = urlsDict.CreateArray("CFBundleURLSchemes");
            urlsArray.AddString(string.Format("vk{0}", ShareConfig.VKontakteAppId));
            urlsArray.AddString(string.Format("fb{0}", ShareConfig.FacebookAppId));
            urlsArray.AddString(string.Format("ok{0}", ShareConfig.OdnoklassnikiAppId));
            File.WriteAllText(plistPath, plist.WriteToString());
        }

        // Подключаем сторонние фреймворки
        private static void AddFrameworks(string path)
        {
            string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
            PBXProject proj = new PBXProject();
            var file = File.ReadAllText(projPath);
            proj.ReadFromString(file);
            string target = proj.TargetGuidByName("Unity-iPhone");
            string[] files = Directory.GetDirectories(fullFrameworksPath);
            List<string> frameworks = new List<string>();
            for (int i = 0; i < files.Length; i++)
            {
               frameworks.Add(files[i]);
            }
            foreach (string framework in frameworks)
            {
                string fileName = GetDirName(framework);
                CopyDirectory(framework, Path.Combine(path, "Frameworks/" + fileName));
                string name = proj.AddFile("Frameworks/" + fileName, "Frameworks/" + fileName, PBXSourceTree.Source);
                proj.AddFileToBuild(target, name);
            }
            string str = Path.Combine(path, Path.Combine("Frameworks", frameworksPath));
            try
            {

                Directory.Delete(str, true);
            }
            catch
            {
            }
            proj.SetBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(inherited)");
            proj.AddBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/Libraries");
            AddUsrLib(proj, target, "libc++.dylib");
            AddUsrLib(proj, target, "libz.dylib");
            proj.SetBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(SRCROOT)/Frameworks");
            proj.AddBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(inherited)");
            proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");
            File.WriteAllText(projPath, proj.WriteToString());
        }

        private static string GetDirName(string path)
        {
            for (var i = path.Length; --i >= 0;)
            {
                var ch = path[i];
                if (ch == System.IO.Path.DirectorySeparatorChar ||
                    ch == System.IO.Path.AltDirectorySeparatorChar ||
                    ch == System.IO.Path.VolumeSeparatorChar)
                {
                    int index = i + 1;
                    return path.Substring(index, path.Length -index);
                }
            }
            return path;
        }

        static void AddUsrLib(PBXProject proj, string targetGuid, string framework)
        {
            string fileGuid = proj.AddFile("usr/lib/" + framework, "Frameworks/" + framework, PBXSourceTree.Sdk);
            proj.AddFileToBuild(targetGuid, fileGuid);
        }

        private static void CopyDirectory(string sourceDirName, string destDirName) {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }
            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }
            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, false);
            }
            foreach (DirectoryInfo subdir in dirs)
            {
                // Create the subdirectory.
                string temppath = Path.Combine(destDirName, subdir.Name);

                // Copy the subdirectories.
                CopyDirectory(subdir.FullName, temppath);
            }
        }

        static void CopyAndReplaceDirectory(string sourcePath, string distinationPath)
        {
            if (Directory.Exists(distinationPath))
                Directory.Delete(distinationPath);
            if (File.Exists(distinationPath))
                File.Delete(distinationPath);
            Directory.CreateDirectory(distinationPath);
            foreach (var file in Directory.GetFiles(sourcePath))
                File.Copy(file, Path.Combine(distinationPath, Path.GetFileName(file)));
            foreach (var dir in Directory.GetDirectories(sourcePath))
                CopyAndReplaceDirectory(dir, Path.Combine(distinationPath, Path.GetFileName(dir)));
        }
    }
}
#endif