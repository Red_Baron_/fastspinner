﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace SimpleLocalizator {
	public class MultiLanguageTextDoubleSwitcher : MultiLanguageComponent {
		[System.Serializable]
		public struct SwitchObject {
			public Language lang;
			public Text text;
		}

		[SerializeField] ScrollRect scrollRect;
		[SerializeField] List<SwitchObject> objects = new List<SwitchObject>();

		protected override void Refresh() {
			for (int i = 0; i < objects.Count; i++) {
				bool enable = objects [i].lang == currentLanguage;
				objects [i].text.gameObject.SetActive(enable);
				if (enable)
					scrollRect.content = objects [i].text.rectTransform;
			}
		}

	}
}

