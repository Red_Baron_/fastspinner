﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SimpleLocalizator {
	public class LanguageManager : MonoBehaviour {
		#region Data
		[SerializeField] bool autoDetectLanguage=true;
		[SerializeField] Language defaultLanguage = Language.english;
		[SerializeField] Language _currentLanguage;
		static string _loc = string.Empty;
		static LanguageManager _instance;
		static LanguageManager instance {
			get {
				if (_instance == null) {
					_instance = FindObjectOfType<LanguageManager> ();
					if (_instance == null) {
						GameObject obj = new GameObject ("LanguageManager");
						DontDestroyOnLoad (obj);
						_instance = obj.AddComponent<LanguageManager> ();
					}
				}
				return _instance;
			}
			set {
				_instance = value;
			}
		}
		static List<Label> labels {
			get {
				return LabelsData.labels;
			}
		}
		#endregion

		#region Interface
		public static string loc {
			get {
				return _loc;
			}
		}

		public static Language currentLanguage {
			get {
				return instance._currentLanguage;
			}
			private set {
				instance._currentLanguage = value;
				if (onLanguageChanged != null)
					onLanguageChanged ();
			}
		}

		public static Action onLanguageChanged;

		public static Label GetLabel (int labelID) {
			for (int i = 0; i < labels.Count; i++) {
				if (labels [i].id == labelID) {
					return labels [i];
				}
			}
			return LabelsData.defaultLabel;
		}

		public static string GetString(int labelID) {
			for (int i = 0; i < labels.Count; i++) {
				if (labels [i].id == labelID) {
					return labels [i].GetString (currentLanguage);
				}
			}
			return LabelsData.defaultLabel.GetString (currentLanguage);
		}
		#endregion

		#region Methods
		Language GetSystemLanguage() {
			switch (Application.systemLanguage) {
			case SystemLanguage.Ukrainian:
				return Language.russian;
			case SystemLanguage.Russian:
				return Language.russian;
			case SystemLanguage.English:
				return Language.english;
			default:
				return defaultLanguage;
			}
		}

		void OnEnable() {
			if (_instance == null) {
				instance = this;
			} else if (instance != this) {
				Destroy (this);
			}
		}

		void Start() {
			if (autoDetectLanguage) {
				currentLanguage = GetSystemLanguage ();
			} else {
				currentLanguage = currentLanguage;
			}
		}
		#endregion
	}
}

