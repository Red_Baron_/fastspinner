﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SimpleLocalizator {
	[RequireComponent(typeof(TextMesh))]
	public class MultiLanguageTextMesh : MultiLanguageTextBase {
		TextMesh _text;
		TextMesh text {
			get {
				if (_text == null)
					_text = GetComponent<TextMesh> ();
				return _text;
			}
		}

		protected override void VisualizeString() {
			text.text = currentString;
		}
	}
}

