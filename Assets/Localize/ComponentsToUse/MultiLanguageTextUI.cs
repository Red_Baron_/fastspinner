﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SimpleLocalizator {
	[RequireComponent(typeof(Text))]
	public class MultiLanguageTextUI : MultiLanguageTextBase {
		Text _text;
		public Text text {
			get {
				if (_text == null)
					_text = GetComponent<Text> ();
				return _text;
			}
		}

		protected override void VisualizeString() {
			if (text!=null)
				text.text = currentString;
		}
	}
}


