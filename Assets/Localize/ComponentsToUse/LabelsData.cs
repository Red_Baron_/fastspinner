﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SimpleLocalizator {
	[CreateAssetMenu(fileName="LabelsData", menuName="SimpleLocalizator/LabelsData")]
	public class LabelsData : ScriptableObject {
		#if UNITY_EDITOR
		public enum EncodingType {UTF8, ANSI};
		[SerializeField] EncodingType encoding;
		#endif
		[SerializeField] List<Label> _labels=new List<Label>();
		Label _defaultLabel = new Label (-1, "not translated");

		public static Label defaultLabel {
			get {
				return instance._defaultLabel;
			}
		}

		public static List<Label> labels {
			get {
				return instance._labels;
			}
			private set {
				instance._labels = value;
			}
		}

		#if UNITY_EDITOR
		public void Load() {
			labels = TranslationsSaver.LoadFromCSV ();
			MultiLanguageTextBase[] texts = FindObjectsOfType<MultiLanguageTextBase> ();
			for (int i = 0; i < texts.Length; i++) {
				texts [i].OnValidate ();
			}
		}

		public void Save() {
			TranslationsSaver.SaveToCSV (labels, encoding == EncodingType.UTF8 ? Encoding.UTF8 : Encoding.Default);
		}

		public void ClearLabels() {
			labels.Clear ();
		}
		#endif



		static LabelsData _instance;
		public static LabelsData instance {
			get {
				if (_instance==null) {
					_instance = (LabelsData)Resources.Load ("LabelsData");
					if (_instance == null) {
						_instance = LabelsData.CreateInstance<LabelsData> ();
						//Debug.Log ("LabelsData: loaded instance from resources is null, created instance");
					} else {
						//Debug.Log ("LabelsData: loading instance from resources is successfull!");
					}
				}
				return _instance;
			}
		}
	}

	#if UNITY_EDITOR
	[CustomEditor(typeof(LabelsData))]
	public class LabelsDataEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			LabelsData myScript = (LabelsData)target;
			if(GUILayout.Button("Load from CSV"))
			{
				myScript.Load ();
			}
			if(GUILayout.Button("Save to CSV"))
			{
				myScript.Save ();
			}
			if(GUILayout.Button("Clear labels"))
			{
				myScript.ClearLabels ();
			}
			EditorUtility.SetDirty (target);
		}
	}
	#endif
}


