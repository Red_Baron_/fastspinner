﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;


namespace SimpleLocalizator {
	public static class TranslationsSaver{
		#region Data
		static string fileName = "translations.csv";
		static string filePath {
			get {
				return Path.Combine (Path.Combine(Application.dataPath, "Resources"), fileName);
			}
		}
		#endregion

		#region Methods
		static List<Language> ParseHeader(string header, int enumCount) {
			List<Language> result = new List<Language> ();
			string[] strings = SplitCSVString (header, enumCount + 1);
			object current = null;
			for (int i = 1; i < strings.Length; i++) {
				try {
					current = Enum.Parse (typeof(Language), strings [i]);
					if (current != null) {
						result.Add ((Language)current);
					}
				}
				catch {}
			}
			return result;
		}

		static string[] SplitCSVString(string str, int count) {
			char[] delimiter = { ';' };
			string[] result = str.Split (delimiter, count);
			for (int i = 0; i < result.Length; i++) {
				if (result[i].Contains(";"))
					result[i]=result[i].Replace(";", string.Empty);
			}
			return result;
		}

		static Label ParseLabel(string str, List<Language> langs) {
			string[] strings = SplitCSVString (str, langs.Count + 1);
			int id = 0;
			if (Int32.TryParse(strings[0], out id)) {
				Label result = new Label (id);
				int langCounter = 0;
				for (int i = 1; i < strings.Length; i++) {
					result.SetString (langs [langCounter], strings [i]);
					langCounter++;
					if (langCounter >= langs.Count)
						break;
				}
				//Debug.Log ("Parse label: success. Label:" + result.ToString ());
				return result;
			}
			return null;
		}

		static StringBuilder ListToCSV(List<Label> labels) {
			StringBuilder result = new StringBuilder ();
			int enumCount = Enum.GetNames(typeof(Language)).Length;
			Array valuesAsArray = Enum.GetValues(typeof(Language));
			string current = "id;";
			for (int i = 0; i < enumCount; i++) {
				current += (valuesAsArray.GetValue (i)).ToString() + ";"; //adding header
			}
			result.AppendLine (current);
			for (int i = 0; i < labels.Count; i++) {
				current = labels [i].id.ToString () + ";";
				for (int j = 0; j< enumCount; j++) {
					current += labels[i].GetString((Language)(valuesAsArray.GetValue (j))) + ";"; //adding fields
				}
				//Debug.Log (labels[i].ToString());
				result.AppendLine (current);
			}
			return result;
		}

		static List<Label> StringToList(string str) {
			StringReader r = new StringReader (str);
			int linesCount = str.LinesCount ();
			int enumCount = Enum.GetNames(typeof(Language)).Length;
			string currentStr = r.ReadLine ();
			List<Language> langs = ParseHeader (currentStr, enumCount);
			List<Label> result = new List<Label> ();
			Label currentLabel=null;
			for (int i = 1; i < linesCount; i++) {
				currentStr = r.ReadLine ();
				if (currentStr != null) {
					currentLabel = ParseLabel (currentStr, langs);
					if (currentLabel != null)
						result.Add (currentLabel);
				}
			}
			return result;
		}
		#endregion



		#region Interface
		public static void SaveToCSV(List<Label> labels, Encoding encoding) {
			try {
				File.WriteAllText(filePath, ListToCSV (labels).ToString(), encoding);
				Debug.Log("Translation Saver: csv saved successfully!");
			}
			catch (Exception ex) {
				Debug.LogError("Translation Saver: can't save csv file:"+ex.ToString());
			}
		}

		public static List<Label> LoadFromCSV() {
			try {
				string str = File.ReadAllText (filePath);
				List<Label> result = StringToList (str);
				Debug.Log("Translation Saver: csv loaded successfully!");
				return result;
			}
			catch (Exception ex) {
				Debug.LogError("Translation Saver: can't read csv file:"+ex.ToString());
				return new List<Label> ();
			}
		}
		#endregion

	}


}


