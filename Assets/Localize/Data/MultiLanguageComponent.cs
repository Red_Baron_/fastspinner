﻿using UnityEngine;


namespace SimpleLocalizator {
	public class MultiLanguageComponent : MonoBehaviour {
		[SerializeField] Language _currentLanguage;

		protected Language currentLanguage {
			get {
				return _currentLanguage;
			}
			set {
				_currentLanguage = value;
				Refresh ();
			}
		}

		public void OnValidate() {
			currentLanguage = _currentLanguage;
		}

		void OnEnable() {
			OnLanguageRefresh ();
			LanguageManager.onLanguageChanged += OnLanguageRefresh;
		}

		void OnDisable() {
			LanguageManager.onLanguageChanged -= OnLanguageRefresh;
		}

		void OnLanguageRefresh() {
			currentLanguage = LanguageManager.currentLanguage;
		}

		protected virtual void Refresh() {
		}
	}
}