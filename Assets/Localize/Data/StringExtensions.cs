﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SimpleLocalizator {
	public static class StringExtensions{
		public static int LinesCount(this string str)
		{
			if (str == null || str == string.Empty)
				return 0;
			int len = str.Length;
			int c = 0;
			for (int i = 0; i < len; i++) {
				if (str [i] == '\n')
					c++;
			}
			return c + 1;
		}
	}
}
