﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace SimpleLocalizator {
	[System.Serializable]
	public class Label{
		#region Data
		[SerializeField] int _id;
		[SerializeField] TranslationsLibrary translations = new TranslationsLibrary ();
		#endregion


		#region Interface
		public int id {
			get {
				return _id;
			}
			private set {
				_id = value;
			}
		}

		public Label(int id, string defaultText="") {
			this.id = id;
			int enumCount = Enum.GetNames(typeof(Language)).Length;
			Array valuesAsArray = Enum.GetValues(typeof(Language));
			for (int i = 0; i < enumCount; i++) {
				translations.Set ((Language)(valuesAsArray.GetValue(i)), defaultText);
			}
		}

		public string GetString(Language language) {
			return translations.Get(language);
		}

		public void SetString(Language language, string str) {
			translations.Set (language, str); 
		}
		#endregion

		#region Methods
		public override string ToString ()
		{
			return "Label (id:" + id.ToString () + ")."+translations.ToString();
		}
		#endregion
	}
}


