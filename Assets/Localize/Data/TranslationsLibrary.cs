﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SimpleLocalizator {
	[System.Serializable]
	public class TranslationsLibrary{
		[SerializeField] List<Translation> translations = new List<Translation> ();

		public string Get(Language key) {
			for (int i = 0; i < translations.Count; i++) {
				if (translations [i].key == key) {
					return translations [i].value;
				}
			}
			translations.Add (new Translation(key, string.Empty));
			return translations [translations.Count - 1].value;
		}

		public void Set (Language key, string value) {
			for (int i = 0; i < translations.Count; i++) {
				if (translations [i].key == key) {
					translations [i] = new Translation (key, value);
					return;
				}
			}
			translations.Add (new Translation(key, value));
		}

		public override string ToString ()
		{
			return string.Format ("TranslationsLibrary: ("+translations.ToString()+")");
		}
	}


}


