﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleLocalizator {
	public class MultiLanguageTextBase : MonoBehaviour{
		#region Unity scene settings
		[SerializeField] int _labelID;
		[SerializeField] bool toUpper=false;
		#endregion

		#region Data
		[SerializeField] Language _currentLanguage;
		string _currentString=string.Empty;
		Label _currentLabel;
		//Language oldLanguage;
		//int oldLabelID=-1;

		Label currentLabel {
			get {
				if (_currentLabel==null)
					currentLabel = LanguageManager.GetLabel (labelID);
				return _currentLabel;
			}
			set {
				_currentLabel = value;
				RefreshString ();
			}
		}
			
		Language currentLanguage {
			get {
				return _currentLanguage;
			}
			set {
				//oldLanguage = currentLanguage;
				_currentLanguage = value;
				RefreshString ();
			}
		}
			
		protected string currentString {
			get {
				return _currentString;
			}
			private set {
				_currentString = value;
				VisualizeString ();
			}
		}
		#endregion

		#region Interface

		public int labelID {
			get {
				return _labelID;
			}
			set {
				//oldLabelID = labelID;
				_labelID = value;
				currentLabel = LanguageManager.GetLabel (labelID);
			}
		}
		#endregion

		#region Methods
		void RefreshString() {
			currentString = currentLabel.GetString (currentLanguage);
			if (toUpper)
				currentString = currentString.ToUpper ();
		}

		public void OnValidate() {
			OnLabelsRefresh ();
			currentLanguage = _currentLanguage;
		}

		void OnEnable() {
			OnLabelsRefresh ();
			OnLanguageRefresh ();
			LanguageManager.onLanguageChanged += OnLanguageRefresh;
		}

		void OnDisable() {
			LanguageManager.onLanguageChanged -= OnLanguageRefresh;
		}

		void OnLanguageRefresh() {
			currentLanguage = LanguageManager.currentLanguage;
		}

		void OnLabelsRefresh() {
			currentLabel = LanguageManager.GetLabel (labelID);
		}

		protected virtual void VisualizeString() {
			
		}
		#endregion
	}
}

