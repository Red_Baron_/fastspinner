#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("1+yN3vnCBNMbVubwmYIR0xWhGBOvtPWyGKH4ZZ8QXUx5Mb1rwfjJ9u3TOgprQ1j0Drb5g0IxKXaJuFGNLGbhCXxA9p1Z692mSjCsa+pt+VqauZSTl5eVkJOEjPrm5uLhqL295RmLG0xr2f5nlTmwopB6iqxqwptBjQNJjNXCeZd/zOsWv3mkMMXex36ig5SRx5aYgZjT4uL+97Lb/PG8o+XlvPPi4v73vPH9/73z4uL+9/HzlKKdlJHHj4GTk22Wl6KRk5Ntoo+UkcePnJaEloa5QvvVBuSbbGb5H+L+97LA/f3mstHTooyFn6Kkoqag/Pay8f389vvm+/384bL99LLn4ff1HZomsmVZPr6y/eIkrZOiHiXRXZeSkRCTnZKiEJOYkBCTk5J2AzubEJOSlJu4FNoUZfH2l5OiE2CiuJQliS8B0LaAuFWdjyTfDszxWtkShcs1l5vuhdLEg4zmQSUZsanVMUf9msyiEJODlJHHj7KWEJOaohCTlqLrsvPh4ef/9+Gy8/Hx9+Lm8/zx9+Dz8eb78fey4ebz5vf/9/zm4byi20rkDaGG9zPlBlu/kJGTkpMxEJPoohCT5KKclJHHj52Tk22WlpGQkzpO7LCnWLdHS51E+UYwtrGDZTM+5vv0+/Hz5vey8Ouy8/zrsuLz4OYShrlC+9UG5JtsZvkfvNI0ZdXf7b2iE1GUmrmUk5eXlZCQohMkiBMhlpSBkMfBo4Gig5SRx5aYgZjT4uLm+v3g++bro4SihpSRx5aRgZ/T4qGkyKLwo5mim5SRx5aUgZDHwaOBW4vgZ8+cR+3NCWC3kSjHHd/Pn2P2p7GH2YfLjyEGZWQODF3CKFPKwrZweUMl4k2d13O1WGP/6n91J4WF8P73suHm8/z28+D2sub34P/hsvOkC96/6iV/HglOYeUJYORA5aLdU+L+97LR9+Dm+/T78fPm+/38stPnlX7vqxEZwbJBqlYjLQjdmPltuW6EooaUkceWkYGf0+Li/veywP395rLR06IQk7Cin5SbuBTaFGWfk5OTUvGh5WWolb7EeUids5xIKOGL3Se80jRl1d/tmsyijZSRx4+xloqihB3hE/JUicmbvQAgatbaYvKqDIdnS6TtUxXHSzULK6DQaUpH4wzsM8C4FNoUZZ+Tk5eXkqLwo5mim5SRx/73stv88byjtKK2lJHHlpmBj9Pisv30sub697Lm+vf8svPi4v778fOdD69hudu6iFpsXCcrnEvMjkRZr/v0+/Hz5vv9/LLT5+b6/eD75uujOTHjANXBx1M9vdMhamlx4l90Md6iEJYpohCRMTKRkJOQkJOQop+Um8D3/vvz/PH3sv38sub6++Gy8ffgjRcRF4kLr9WlYDsJ0hy+RiMCgEonqD9mnZySAJkjs4S85keun0nwhJ+Um7gU2hRln5OTl5eSkRCTk5LOsvP89rLx9+Dm+/T78fPm+/38suK0oraUkceWmYGP0+Li/vey0ffg5iOiyn7IlqAe+iEdj0z34W31zPcuvrLx9+Dm+/T78fPm97Li/f778esHDOieNtUZyUaEpaFZVp3fXIb7Q6ego6aioaTIhZ+hp6Kgoqugo6aiwjgYR0h2bkKblaUi5+ez");
        private static int[] order = new int[] { 25,56,17,50,35,16,15,9,36,29,30,15,43,21,14,34,59,52,33,40,46,27,28,54,39,32,37,52,59,36,40,46,53,42,48,35,51,43,52,57,46,55,58,56,50,55,59,48,57,53,52,53,54,57,55,59,56,59,58,59,60 };
        private static int key = 146;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
