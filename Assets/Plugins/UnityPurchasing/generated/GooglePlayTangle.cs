#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("D4yCjb0PjIePD4yMjUZNlPEjGsF5UK1VQ4r5lvJI2dhAKUPcJ1f6xQtymqa+MzsuKSp8plBK7gfeHc8hvQ+Mr72Ai4SnC8ULeoCMjIyIjY6KAlzOo0GyFGo70/S8fgpsunAlRru9XLXHdcB3g2Svn5JWnpotRNzwfDK+zsH3NCoDY9BP+XnAyuPjVh/9p0T/ljyeZ/eJZHyA3pjT8p7RgQZSEnnDGiCMOxyyZdW8pvIHnbej7DSvXxgV3vfEbsM8kMX+R3nTT0q56jF9D+O0/qstq7cscBUPt6HNQTcgEzZrvUiAXoBuV+9ddJW+iM7jvji/AdjwvG/NWm4bZ3xUbDZnzDqGoljWF20CFEBjirtXU97R5xrZoiYG3qwc76fcFI+OjI2M");
        private static int[] order = new int[] { 1,11,2,11,8,12,6,8,10,9,10,12,13,13,14 };
        private static int key = 141;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
